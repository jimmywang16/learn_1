#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<assert.h>

struct ListNode
{
	struct ListNode* _next;
	int _val;

	ListNode(int x)
		:_val(x)
		,_next(NULL)
	{}

	void Print()
	{
		cout << _val << endl;
	}
};

struct ListNode* BuyNode(int x)
{
	struct ListNode* newnode = (struct ListNode*)malloc(sizeof(struct ListNode));
	if (newnode == NULL)
	{
		perror("malloc failed");
		return NULL;
	}
	newnode->_val = x;
	newnode->_next = NULL;

	return newnode;
}

int main()
{
	struct ListNode* pln1 = BuyNode(1);// malloc free都是函数
	struct ListNode* pln2 = BuyNode(2);
	
	// 开空间+调用构造函数初始化
	ListNode* n1 = new ListNode(1);
	ListNode* n2 = new ListNode(2); // new delete都是操作符
	n1->Print();
	n2->Print();
	delete n1;
	delete n2;

	return 0;
}