#pragma once
#include<iostream>
#include<assert.h>

using namespace std;
#include "ReverseIterator.h"

namespace a
{
	template <class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		typedef ReverseIterator<iterator, T&, T*> reverse_iterator;
		typedef ReverseIterator<const_iterator, const T&, const T*> const_reverse_iterator;

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin()const
		{
			return _start;
		}
		const_iterator end()const
		{
			return _finish;
		}

		vector()
			:_start(nullptr)
			,_finish(nullptr)
			,_endof_storage(nullptr)
		{}

		vector(size_t n, const T& val = T())
			:_start(nullptr)
			,_finish(nullptr)
			,_endof_storage(nullptr)
		{
			resize(n, val);
		}

		vector(int n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endof_storage(nullptr)
		{
			resize(n, val);
		}
		
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}/**/


		~vector()
		{
			delete[] _start;
			_start = _finish = _endof_storage = nullptr;
		}

		vector(const vector<T>& v)
			: _start(nullptr)
			, _finish(nullptr)
			, _endof_storage(nullptr)
		{
			_start = new T[v.capacity()];
			/*memcpy(_start, v._start, sizeof(T)*v.size());*/
			
			for (size_t i = 0; i < v.size(); i++)
			{
				_start[i] = v._start[i];
			}

			_finish = _start + v.size();
			_endof_storage = _start + v.capacity();
		}
/*
		vector(const vector<T>& v)
			: _start(nullptr)
			, _finish(nullptr)
			, _endof_storage(nullptr)
		{
			reserve(v.capacity());
			for (auto e : v)
			{
				push_back(e);
			}
		}
*/
		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endof_storage, v._endof_storage);
		}

		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				T* tmp = new T[n];
				size_t oldsize = size();
				if (_start)
				{
					//memcpy(tmp, _start, sizeof(T) * oldsize);
					/*
					vector是深拷贝。
					如果vector空间上存的对象是string的数组，使用memcpy会导致string对象的浅拷贝
					—— —— 指向同一段空间调用两次析构函数。
					*/
					for (int i = 0; i < oldsize; i++)
					{
						tmp[i] = _start[i];
					}
					/*解决方案：T是string这样深拷贝的类，调用的是string赋值重载，实现string对象的深拷贝
					*/
					delete[] _start;
				}
				_start = tmp;
				_finish = _start + oldsize;
				_endof_storage = tmp + n; 
			}
		}

		void resize(size_t n, const T& val = T())
		{
			if (n < size())
			{
				_finish = _start + n;
			}
			else
			{
				reserve(n);
				while (_finish != _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
		}

		void push_back(const T& x)
		{
			if (_finish == _endof_storage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}
			*_finish = x;
			_finish++;
		}

		void pop_back()
		{
			--_finish;//这儿end函数调用返回的是一个临时变量，所以不能被修改,后面断言问题应该是正常的
		}

		T& operator[](size_t pos)
		{
			assert(pos < size());
			return *(_start + pos);
		}
		const T& operator[](size_t pos)const
		{
			assert(pos < size());
			return *(_start + pos);
		}
		size_t size()const
		{
			return _finish - _start;
		}
		size_t capacity()const
		{
			return _endof_storage - _start;
		}

		iterator insert(iterator pos, const T& x)
		{
			assert(pos <= _finish&& pos >= _start);
			if (_finish == _endof_storage)
			{
				size_t len = pos - _start;

				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);

				// 解决pos迭代器失效问题
				pos = _start + len;
			}
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				end--;
			}
			*pos = x;
			_finish++;
			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos <=_finish - 1);

			iterator begin = pos+1;
			while (begin < _finish )
			{
				*(begin - 1) = *begin;
				begin++;
			}
			_finish--;

			return pos;
		}
		
	private:
		iterator _start;
		iterator _finish;
		iterator _endof_storage;
	};


	
}
