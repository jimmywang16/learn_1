#define _CRT_SECURE_NO_WARNINGS 1
#include "vector.h"
#include "list.h"
#include "ReverseIterator.h"
//#include<vector>

namespace a
{
	void test_list_vector()
	{
		cout << "list: _ " << endl;
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);
		lt.push_back(6);

		list<int>::iterator it = lt.begin();
		while (it != lt.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;

		list<int>::reverse_iterator rit = lt.rbegin();
		while (rit != lt.rend())
		{
			cout << *rit << " ";
			++rit;
		}

		cout << endl;
		cout << endl;


		cout << "vector: _ " << endl;

		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);

		vector<int>::iterator it2 = v.begin();
		while (it2 != v.end())
		{
			cout << *it2 << " ";
			it2++;
		}
		cout << endl;


		vector<int>::reverse_iterator rit2 = v.rbegin();
		while (rit2 != v.rend())
		{
			cout << *rit2 << " ";
			++rit2;
		}
		cout << endl;
		//����
		v.pop_back();
		v.pop_back();
		cout << "pop--��" << endl;

		rit2 = v.rbegin();
		while (rit2 != v.rend())
		{
			cout << *rit2 << " ";
			++rit2;
		}
	}

	void test1()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);

		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		vector<int>::reverse_iterator rit = v.rbegin();
		while (rit != v.rend())
		{
			cout << *rit << " ";
			++rit;
		}
	}

}

//void test3()
//{
//	vector<int> v;
//	v.push_back(1);
//	v.push_back(2);
//	v.push_back(3);
//	v.push_back(4);
//	v.push_back(5);
//
//	vector<int>::iterator it = v.begin();
//	while (it != v.end())
//	{
//		cout << *it << " ";
//		it++;
//	}
//}

int main()
{
	a::test_list_vector();
	//a::test1();
	//test3();
	return 0;
}