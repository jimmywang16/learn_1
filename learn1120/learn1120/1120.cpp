#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include <iostream>

class A
{
public:
	A(int a = 0)
		: _a(a)
	{
		cout << "A():" << this << endl;
	}
	~A()
	{
		cout << "~A():" << this << endl;
	}
private:
	int _a;
};

//int main()
//{
//
//	A* p1 = (A*)malloc(sizeof(A));
//	new(p1)A; 
//	//注意：如果A类的构造函数有参数时，此处需要传参
//
//	p1->~A();
//	free(p1);
//	A* p2 = (A*)operator new(sizeof(A));
//	new(p2)A(10);
//	p2->~A();
//	operator delete(p2);
//
//	return 0;
//}
int main()
{
	A* p1 = (A*)malloc(sizeof(A));
	// 显式调用构造函数
	// 注意：如果A类的构造函数有参数时，此处需要传参
	new(p1)A(1);// 定位new/replacement-new
	// p1是内置类型，不会自动调用构造和析构
	p1->~A();
	
	return 0;
}