#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	int data;
	struct Node* next;
}Node;

Node* createnode(int data)
{
	Node* newnode = (Node*)malloc(sizeof(Node));
	if (newnode == NULL)
	{
		printf("错误：分配错误\n");
		return NULL;
	}

	newnode->data = data;
	newnode->next = NULL;

	return newnode;
}


void insertnode(Node** head, Node* newnode, int index)
{
	if (index == 0)
	{
		newnode->next = *head;
		*head = newnode;

		return;
	}

	Node* prev = NULL;
	Node* cur = *head;
	int i = 0;

	while (cur != NULL && i < index)
	{
		prev = cur;
		cur = cur->next;
		i++;
	}
	if (cur == NULL)//尾插
	{
		prev->next = newnode;

	}
	else//插到中间
	{
		prev->next = newnode;
		newnode->next = cur;
	}
}

void display_list(Node* head)
{
	printf("打印链表内容:\n");
	Node* cur = head;
	while (cur)
	{
		printf("%d->", cur->data);

		cur = cur->next;
	}
	printf("NULL");
	printf("\n");
}

int main()
{
	Node* head = NULL;

	Node* node1 = createnode(1);
	Node* node2 = createnode(2);
	Node* node3 = createnode(3);
	Node* node4 = createnode(4);
	Node* node5 = createnode(4);
	Node* node6 = createnode(6);

	insertnode(&head, node1, 0);
	insertnode(&head, node2, 1);
	insertnode(&head, node3, 2);
	insertnode(&head, node4, 3);
	insertnode(&head, node5, 4);
	insertnode(&head, node6, 6);

	display_list(head);

	return 0;
}


//#include<stdio.h>
//#include<stdlib.h>
////定义节点结构体
//typedef struct Node
//{
//	//节点数据
//	int data;
//	//指向下一个节点的指针
//	struct Node* next;
//}Node;
////创建节点函数
//Node* createNode(int data)
//{
//	//动态申请内存 malloc
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	if (newNode == NULL)
//	{
//		printf("分配新节点内存失败\n");
//		return NULL;
//	}
//	//分配
//	newNode->data = data;//设置节点数据
//	newNode->next = NULL;//指针指向下一个节点(空指针）
//	return newNode;
//}
//void insetNode(Node** head, Node* newNode, int index)
//{
//	if (index == 0)
//	{
//		//插入到链表头部
//		newNode->next = *head;
//		*head = newNode;
//		return;
//	}
//	Node* previous = NULL;
//	Node* current = *head;
//	int i = 0;
//	while (current != NULL && i < index)
//	{
//		previous = current;
//		current = current->next;
//		i++;
//	}
//	if (current == NULL)
//	{
//		//插入到链表尾部
//		previous->next = newNode;
//		newNode->next = current;
//	}
//}
////显示链表数据
//void displayList(Node* head)
//{
//	printf("链表内容：\n");
//	Node* current = head;
//	while (current != NULL)
//	{
//		printf("%d->", current->data);
//		current = current->next;
//	}
//	printf("NULL");
//	printf("\n");
//}
//int main()
//{
//	Node* head = NULL;
//	//创建空链表
//	Node* node1 = createNode(10);
//	Node* node2 = createNode(20);
//	Node* node3 = createNode(30);
//	//插入节点到链表
//	insetNode(&head, node1, 0);
//	insetNode(&head, node2, 1);
//	insetNode(&head, node3, 2);
//	//显示链表内容
//	displayList(head);
//
//	return 0;
//}