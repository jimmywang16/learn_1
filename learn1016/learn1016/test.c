#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

typedef int BTDataType;

typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;

BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL)
	{
		perror("malloc fail");
		return NULL;
	}

	node->data = x;
	node->left = NULL;
	node->right = NULL;

	return node;
}

BTNode* CreateTree()
{
	BTNode* n1 = BuyNode(1);
	BTNode* n2 = BuyNode(2);
	BTNode* n3 = BuyNode(3);
	BTNode* n4 = BuyNode(4);
	BTNode* n5 = BuyNode(5);
	BTNode* n6 = BuyNode(6);
	BTNode* n7 = BuyNode(7);

	n1->left = n2;
	n1->right = n4;

	n2->left = n3;
	n4->left = n5;
	n4->right = n6;

	n6->left = n7;

	return n1;
}

void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//int size = 0;
void TreeSize(BTNode* root, int* psize)
{
	if (root == NULL)
	{
		return;
	}

	(*psize)++;
	TreeSize(root->left, psize);
	TreeSize(root->right, psize);
}

int Treesize1(BTNode* root)
{
	return (root == NULL) ? 0 :
		Treesize1(root->left)
		+ Treesize1(root->right)
		+ 1;
}



int TreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;

	//时间复杂度O（N^2）,每次调用都不保存数据
	//return TreeHeight(root->left) > TreeHeight(root->right)
	//	? TreeHeight(root->left) + 1 : TreeHeight(root->right) + 1;

	int leftheight = TreeHeight(root->left);
	int rightheight = TreeHeight(root->right);

	return leftheight > rightheight ? leftheight + 1 : rightheight + 1;
}

int TreeLevel(BTNode* root, int k)
{
	assert(k);

	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}

	int leftk = TreeLevel(root->left, k - 1);
	int rightk = TreeLevel(root->right, k - 1);

	return leftk + rightk;
}

BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;

	if (root->data == x)
		return root;

	BTNode* left = BinaryTreeFind(root->left, x);
	if (left != NULL)
		return left;

	BTNode* right = BinaryTreeFind(root->right, x);
	if (right != NULL)
		return right;

	return NULL;
}

int main()
{
	BTNode* root = CreateTree();
	PreOrder(root);
	printf("\n");

	InOrder(root);
	printf("\n");

	PostOrder(root);
	printf("\n");

	int size1 = 0;
	TreeSize(root, &size1);
	printf("TreeSize is %d\n", size1);

	printf("Treesize is also %d\n", Treesize1(root));
	printf("Treeheight is also %d\n", TreeHeight(root));


	printf("Treelevel is also %d\n", TreeLevel(root, 3));
	//使用前对size进行初始化
	///*size = 0;
	//TreeSize(root);
	//printf("TreeSize is %d\n", size);*/

	return 0;
}