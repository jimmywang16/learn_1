#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stdlib.h>
using namespace std;

class Stack
{
public:
	Stack()
	{
		_a = (int*)malloc(sizeof(int)* 4);
		if (_a == nullptr)
		{
			perror("malloc fail");
			return;
		}

		_top = 0;
		_capacity = 4;
	}

	Stack(int capacity )
	{
		_a = (int*)malloc(sizeof(int) * capacity);
		if (_a == nullptr)
		{
			perror("malloc fail");
			return;
		}

		_top = 0;
		_capacity = capacity;
	}

	~Stack()
	{
		cout << "use ~Stack()" << endl;
		free(_a);
		_a = nullptr;
		_capacity = _top = 0;
	}

private:
	int* _a;
	int _top;
	int _capacity;
};

class Date
{
public:
	Date(int year = 2024, int month = 11, int day = 12)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	Date(const Date& d)
	{
		cout << "use Date(const Date& d) " << endl;

		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	/*Stack st;

	Stack st2;*/

	Date d1;
	Date d2(d1);

	return 0;
}