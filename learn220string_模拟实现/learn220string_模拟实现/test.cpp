#define _CRT_SECURE_NO_WARNINGS 1

#include "string.h"

void test1()
{
	a::string s1("123465");
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
	cout << s1.c_str() << endl;
	a::string s2;
	s2 = s1;
	cout << s2.c_str() << endl;
	cout << s2.size() << endl;
	cout << s2.capacity() << endl;
	s2.reserve(20);
	cout << s2.capacity() << endl;

}
void test2()
{
	a::string s1("yourmajesty");
	a::string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
	s1.push_back('!');
	s1.push_back('!');
	s1.push_back('!');
	s1.push_back('!');
	s1.push_back('!');
	cout << endl;

	cout << "size : " << s1.size() << endl;
	cout << "capacity : " << s1.capacity() << endl;
	cout << s1.c_str() << endl;

	s1.append("------Aisin-Gioro Puyi");
	cout << "size : " << s1.size() << endl;
	cout << "capacity : " << s1.capacity() << endl;
	cout << s1.c_str() << endl;

	s1+=" of Great Qing Empire";
	cout << "size : " << s1.size() << endl;
	cout << "capacity : " << s1.capacity() << endl;
	cout << s1.c_str() << endl;

	
	for (int i = 0; i < s1.size(); i++)
	{
		cout << s1[i] ;
	}
	cout << endl;
	a::string s2("Long live China");

	cout << (s1 < s2) << endl;

	cout << s1.find("Aisin-Gioro") << endl;

	a::string s3("11111111111111111111111");
	s3.insert(3, 5, '*');
	cout << s3 << endl;

}
int main()
{
	test2();
	return 0;
}