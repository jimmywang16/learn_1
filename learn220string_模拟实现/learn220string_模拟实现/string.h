#pragma once
#include<iostream>
#include<assert.h>
using namespace std;
namespace a
{
    class string
    {

        friend ostream& operator<<(ostream& cout, const a::string& s);

        friend istream& operator>>(istream& cin, a::string& s);

    public:

        typedef char* iterator;
        typedef const char* const_iterator;

    public:

        string(const char* str = "")
        {
            size_t len = strlen(str);
            char* tmp = new char[len + 1];
            memcpy(tmp, str, len + 1);
            _str = tmp;
            _capacity = len;
            _size = len;
        }

        string(const string& s)
        {
            _size = s._size;
            _capacity = s._capacity;
            char* tmp = new char[_capacity + 1];
            memcpy(tmp, s._str, s._size + 1);
            _str = tmp;
        }

        void swap(string& s)
        {
            std::swap(_str, s._str);
            std::swap(_size, s._size);
            std::swap(_capacity, s._capacity);
        }
        /*
        string& operator=(const string& s)
        {
            _size = s._size;
            _capacity = s._capacity;
            char* tmp = new char[_capacity + 1];
            memcpy(tmp, s._str, s._capacity + 1);
            _str = tmp;
            return *this;
        }*/
        string& operator=(string s)
        {
            swap(s);
            return *this;
        }

        ~string()
        {
            delete[] _str;
            _str = nullptr;
            _size = 0;
            _capacity = 0;
        }

        /////////////////////////////////////////////////////////////
        const char* c_str() const
        {
            return _str;
        }
        

        size_t size()const
        {
            return _size;
        }

        size_t capacity()const
        {
            return _capacity;
        }

        bool empty()const
        {
            return _size == 0;
        }

        void reserve(size_t n)
        {
            if (n > _capacity)
            {
                char* tmp = new char[n + 1];
                memcpy(tmp, _str, _size + 1);
                delete[] _str;
                _str = tmp;
                _capacity = n;
            }
        }

        /*void reserve(size_t n)
        {
            if (n > _capacity)
            {
                cout << "reserve()->" << n << endl;

                char* tmp = new char[n + 1];
                
                memcpy(tmp, _str, _size + 1);

                delete[] _str;
                _str = tmp;
                _capacity = n;
            }
        }*/
        void resize(size_t n, char c = '\0')
        {
            if (n < _capacity)
            {
                _size = n;
                _str[_size] = '\0';
            }
            else
            {
                reserve(n);

                for (size_t i = _size; i < n; i++)
                {
                    _str[i] = c;
                }

                _str[n] = '\0';
                _size = n;
            }
        }

       
        iterator begin()
        {
            return _str;
        }

        iterator end()
        {
            return _str + _size;
        }

        const_iterator begin() const
        {
            return _str;
        }

        const_iterator end() const
        {
            return _str + _size;
        }
    
        void push_back(char c)
        {
            if (_size == _capacity)
            {
                reserve(_size == 0 ? 4 : _capacity*2);
            }
            _str[_size] = c;
            _size++;
            _str[_size] = '\0';
        }

        void append(const char* str)
        {
            size_t len = strlen(str);
            if (len + _size < _capacity || len + _size == _capacity)
            {
                memcpy(_str+_size, str, len + 1);
            }
            else
            {
                reserve(_size + len);
                memcpy(_str + _size, str, len + 1);
            }
            _size += len;
        }
       
        string& operator+=(char c)
        {
            push_back(c);
            return *this;
        }

        string& operator+=(const char* str)
        {
            append(str);
            return *this;
        }

        void clear()
        {
            _str[0] = '\0';
            _size = 0;
        }

        char& operator[](size_t pos)
        {
            assert(pos < _size);

            return *(_str + pos);
        }

        const char& operator[](size_t pos)const
        {
            assert(pos < _size);

            return *(_str + pos);
        }

        //bool operator<(const string& s)
        //{
        //	size_t i1 = 0;
        //	size_t i2 = 0;
        //	while (i1 < _size && i2 < s._size)
        //	{
        //		if (_str[i1] < s._str[i2])
        //		{
        //			return true;
        //		}
        //		else if (_str[i1] > s._str[i2])
        //		{
        //			return false;
        //		}
        //		else
        //		{
        //			++i1;
        //			++i2;
        //		}
        //	}
        //	// "hello" "hello"   false
        //	// "helloxx" "hello" false
        //	// "hello" "helloxx" true

        //	if (i1 == _size && i2 != s._size)
        //	{
        //		return true;
        //	}
        //	else
        //	{
        //		return false;
        //	}

        //	//return i1 == _size && i2 != s._size;
        //	return _size < s._size;
        //}

        bool operator<(const string& s)
        {
            int i1 = 0, i2 = 0;
            while (i1 != _size && i2 != s._size)
            {
                if (_str[i1] < s._str[i2])
                {
                    return true;
                }
                else if (_str[i1] > s._str[i2])
                {
                    return false;
                }
                else
                {
                    ++i1;
                    ++i2;
                }
            }

            if (i1 == _size && i2 != s._size)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
        bool operator==(const string& s) const
        {
            return _size == s._size
                && memcmp(_str, s._str, _size) == 0;
        }

        bool operator<=(const string& s)
        {
            return (*this < s) || (*this == s);
        }

        bool operator>(const string& s)
        {
            return !(*this <= s);
        }

        bool operator>=(const string& s)
        {
            return !(*this < s);
        }

       
        bool operator!=(const string& s)
        {
            return !(*this == s);
        }
        /*size_t find(char ch, size_t pos = 0)
        {
            assert(pos < _size);

            for (size_t i = pos; i < _size; i++)
            {
                if (_str[i] == ch)
                {
                    return i;
                }
            }

            return npos;
        }*/
       size_t find(char c, size_t pos = 0) const
        {
            assert(pos < _size);
/*
            for (size_t i = pos; i < _size; i++)
            {
                if (_str[i] == c)
                {
                    return i;
                }
            }
 */
            size_t i = pos;
            while (i != _size)
            {
                if (_str[i] == c)
                {
                    return i;
                }
                i++;
            }
            return npos;
        }
       
        size_t find(const char* s, size_t pos = 0) const
        {
            assert(pos < _size);
            
            const char* ptr = strstr(_str + pos, s);
            if (ptr)
            {
                return ptr - _str;
            }
            else
            {
                return npos;
            }
        }

        void insert(size_t pos, size_t n, char ch)
        {
            assert(pos < _size);
           
            if (_size + n > _capacity)
            {
                reserve(_size + n);
            }
              
            size_t end = _size;
            while (end >= pos && end != npos)
            {
                _str[end + n] = _str[end];
                end--;
            }
            for (int i = 0; i < n; i++)
            {
                _str[pos + i] = ch;
            }
            _size += n;
        }

        string& insert(size_t pos, const char* str)
        {
        
        }

       
        string& erase(size_t pos, size_t len)
        {
        
        }

        const static size_t npos;

    private:

        char* _str;
        size_t _capacity;
        size_t _size;

    };

    const size_t string::npos = -1;

    ostream& operator<<(ostream& out, const string& s)
    {
        /*for (size_t i = 0; i < s.size(); i++)
        {
            out << s[i];
        }*/

        for (auto ch : s)
        {
            out << ch;
        }

        return out;
    }

    istream& operator>>(istream& in, string& s)
    {
        s.clear();

        char ch = in.get();
        // 处理前缓冲区前面的空格或者换行
        while (ch == ' ' || ch == '\n')
        {
            ch = in.get();
        }

        //in >> ch;
        char buff[128];
        int i = 0;

        while (ch != ' ' && ch != '\n')
        {
            buff[i++] = ch;
            if (i == 127)
            {
                buff[i] = '\0';
                s += buff;
                i = 0;
            }

            //in >> ch;
            ch = in.get();
        }

        if (i != 0)
        {
            buff[i] = '\0';
            s += buff;
        }

        return in;
    }
    //ostream& operator<<(ostream& _cout, const bit::string& s);
    //istream& operator>>(istream& _cin, bit::string& s);
}
   


