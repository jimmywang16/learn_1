#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void find_single_dog(int arr[], int sz, int single_dog[])
{
	int ret = 0;
	int i = 0;				//因为两个相同的是数字按位异或为0
	for (i = 0; i < sz; i++)//那么这个按位异或后的结果肯定是
	{						//两个不相同数字按位异或后的结果--ret
		ret ^= arr[i];
	}
	
	int pos = 0;
	for (i = 0; i < 32; i++)
	{
		if (((ret >> i) & 1) == 1)
		{					//我们去遍历这个ret的bit位，找到为1的bit位位置
			pos = i;		//这个肯定就是那两个数字比特位不同的地方
			break;			//因为相异为1
		}
	}

	for (i = 0; i < sz; i++)
	{									//然后根据bit位不同的位置划分两组
		if (((arr[i] >> pos) & 1) == 1)	//相同比特位的一组
		{
			single_dog[0] ^= arr[i];
		}
		else
		{
			single_dog[1] ^= arr[i];
		}
	}
}

int main()
{
	int arr[] = {1,2,3,4,1,2,3,5};
	int sz = sizeof(arr) / sizeof(arr[0]);
	int single_dog[2] = {0};
	find_single_dog(arr, sz, single_dog);
	printf("%d %d", single_dog[0], single_dog[1]);
	return 0;
}