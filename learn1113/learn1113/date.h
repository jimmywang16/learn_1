#pragma once

#include<iostream>
#include<stdlib.h>
#include<assert.h>
using namespace std;

class Date
{
	//友元函数声明
	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>>(istream& in, Date& d);

public:

	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	Date(int year = 1, int month = 1, int day = 1);
	
	bool operator<(const Date& x) const;
	bool operator==(const Date& x) const;
	bool operator<=(const Date& x) const;
	bool operator>(const Date& x) const;
	bool operator>=(const Date& x) const;
	bool operator!=(const Date& x) const;

	static int Getmonthdays(int year, int month);
	Date& operator+=(int days);
	Date operator+(int days) const;

	Date& operator++();
	Date operator++(int);

	Date& operator-=(int days);
	Date operator-(int days) const;

	Date& operator--();
	Date operator--(int);

	int operator-(const Date& d) const;

	//流插入不能写成成员函数
	//因为Date对象默认占用第一个参数，就是做了左操作数
	//写出来就是下面的样子，不符合使用习惯
	//d2 << cout; // d2.operator<<(cout);
	void operator<<(ostream& out);

	void Print() const // const加在不需要修改——对象成员变量的函数
	{
		cout << _year << " / " << _month << " / " << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

ostream& operator<<(ostream& out, const Date& d);
istream& operator>>(istream& in, Date& d);

