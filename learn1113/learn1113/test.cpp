#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stdlib.h>
using namespace std;

//class Date
//{
//public:
//
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	Date(const Date& d)
//	{
//		cout << "use Date(const Date& d)" << endl;
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//
//	bool operator<(const Date& x)
//	{
//		if (_year < x._year)
//		{
//			return true;
//		}
//		else if (_year == x._year && _month < x._month)
//		{
//			return true;
//		}
//		else if (_year == x._year && _month == x._month && _day < x._day)
//		{
//			return true;
//		}
//
//		return false;
//	}
//
//	Date& operator=(const Date& d)
//	{
//		if (this != &d)
//		{
//			_year = d._year;
//			_month = d._month;
//			_day = d._day;
//		}
//
//		return *this;
//	}
//
//	void Print()
//	{
//		cout << _year << " / " << _month << " / " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
////bool operator<(const Date& x1, const Date& x2)
////{
////	if (x1._year < x2._year)
////	{
////		return true;
////	}
////	else if (x1._year == x2._year && x1._month < x2._month)
////	{
////		return true;
////	}
////	else if (x1._year == x2._year && x1._month == x2._month && x1._day < x2._day)
////	{
////		return true;
////	}
////
////	return false;
////}
//
//int main()
//{
//	Date d1(2024, 2, 26);
//	Date d2(2322, 8, 9);
//
//	Date d3, d4;
//	d3 = d4 = d2;
//
//	
//	d1 < d2;
//	d1.operator<(d2);
//
//	return 0;
//}

#include"date.h"
#include <stdio.h>

void Test()
{
	Date d1(2024, 2, 23);
	Date d2(d1);
	d1.Print();

	++d1;
	d1.Print();
	/*Date d3(d2 + 1222); 
	d2 += 1222;

	d1.Print();
	d3.Print();
	d2.Print();*/


}

void Test2()
{
	Date d1(2024, 11, 13);
	Date d2(d1);

	Date d3(d2);
	d3 += 100;
	d3.Print();

	Date d4(d2);
	d4 -= -100;
	--d4;
	d4.Print();
}

void Test3()
{
	Date d1(2024, 11, 13);

	Date d2(2025, 5, 5);
	Date d3(d2);
	//d2 << cout; // d2.operator<<(cout);

	//d2 << cout;
	cout << d2 << d3;;
	
}
void Test4()
{
	Date d1(2024, 5, 16);
	Date d2(d1);

	Date d3(2026, 2, 26);

	d2 << cout;
	cout << d2 << d3;
}

void Test5()
{
	Date d1(2024, 11, 25);
	const Date d2(2025, 1, 26);

	d1.Print();
	d2.Print();

	int ret = d1 - d2;
	cout << ret << endl;

	/*cin >> d1;

	cout << d1 << d2;*/

	/*Date d3(2026, 13, 26);
	cout << d3;*/
}

int main()
{
	Test5();

	return 0;
}

