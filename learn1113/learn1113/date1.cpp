#define _CRT_SECURE_NO_WARNINGS 1
#include"date.h"

Date::Date(int year, int month, int day)
{
	if (month > 0 && month < 13
		&& day > 0 && day <= Getmonthdays(year, month))
	{
		_year = year;
		_month = month;
		_day = day;
	}
	else
	{
		cout << "非法日期" << endl;
		assert(false);
	}
	
}

bool Date::operator<(const Date& x) const
{
	if (_year < x._year)
	{
		return true;
	}
	else if (_year == x._year && _month < x._month)
	{
		return true;
	}
	else if (_year == x._year && _month == x._month && _day < x._day)
	{
		return true;
	}

	return false;
}

bool Date::operator==(const Date& x) const
{
	return _year == x._year
		&& _month == x._month
		&& _day == x._day;
}
bool Date::operator<=(const Date& x) const
{
	return *this < x || *this == x;
}
bool Date::operator>(const Date& x) const
{
	return !(*this <= x);
}
bool Date::operator>=(const Date& x) const
{
	return !(*this < x);
}
bool Date::operator!=(const Date& x) const
{
	return !(*this == x);
}

int Date::Getmonthdays(int year, int month)
{
	int monthArr[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
	{
		return 29;
	}
	else
	{
		return monthArr[month];
	}
}
Date& Date::operator+=(int days)
{
	if (days < 0)
	{
		return *this -= -days;
	}

	_day += days;
	while (_day > Getmonthdays(_year, _month))
	{
		_day -= Getmonthdays(_year, _month);
		++_month;
		if (_month == 13)
		{
			++_year;
			_month = 1;
		}
	}

	return *this;
}

//Date& Date::operator+=(int days)
//{
//	Date tmp(*this);
//
//	*this = (tmp + days);
//
//	return *this;
//}

//Date& Date::operator+=(int days)
//{
//	_day += days;
//	while (_day > Getmonthdays(_year, _month))
//	{
//		_day -= Getmonthdays(_year, _month);
//		++_month;
//		if (_month == 13)
//		{
//			++_year;
//			_month = 1;
//		}
//	}
//
//	return *this;
//}

Date Date::operator+(int days) const
{
	Date tmp(*this);

	tmp += days;

	return tmp;
	/*tmp._day += days;
	while (tmp._day > Getmonthdays(tmp._year, tmp._month))
	{
		tmp._day -= Getmonthdays(tmp._year, tmp._month);
		++tmp._month;
		if (tmp._month == 13)
		{
			++tmp._year;
			tmp._month = 1;
		}
	}
	return tmp;*/
}
//前置++
Date& Date::operator++()
{
	*this += 1;
	return *this;
}
//后置++
Date Date::operator++(int)
{
	Date tmp = *this;

	*this += 1;
	return tmp;
}

Date& Date::operator-=(int days) 
{
	if (days < 0)
	{
		return *this += -days;
	}

	_day -= days;
	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			_month = 12;
			--_year;
		}
		_day += Getmonthdays(_year, _month);
	}
	return *this;
}
Date Date::operator-(int days) const
{
	Date tmp(*this);
	tmp -= days;
	return tmp;
}

Date& Date::operator--()
{
	*this -= 1;
	return *this;
}
Date Date::operator--(int)
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

// d1 - d2;
int Date::operator-(const Date& d) const
{
	Date max = *this;
	Date min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int n = 0;
	while (min != max)
	{
		++min;
		++n;
	}

	return n * flag;
}

void Date::operator<<(ostream& out)
{
	out << _year << "年" << _month << "月" << _day << "日" << endl;
}

ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;

	return out;
}

istream& operator>>(istream& in, Date& d)
{
	int year, month, day;
	in >> year >> month >> day;

	if (month > 0 && month < 13
		&& day > 0 && day <= Date::Getmonthdays(year, month))
	{
		d._year = year;
		d._month = month;
		d._day = day;
	}
	else
	{
		cout << "非法日期" << endl;
		assert(false);
	}

	return in;
}
