#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//	int sum = 0;
//	int i = 0;
//	int sign = 1;
//	for (i = 1; i <= 1024; i *= 2)
//	{
//		sum += (sign)*i;
//		sign *= -1;
//	}
//	printf("%d", sum);
//	return 0;
//}
#include <stdio.h>

int main() {
	int sum = 0;
	int sign = 1;


	for (int i = 1; i <= 1024; i *= 2) {
		sum += sign * i;
		sign = -sign;
	}

	printf(" %d\n", sum);

	return 0;
}