#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

class Date
{
public:

	/*Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;

	}*/

	Date* operator&()
	{
		cout << "Date* operator&()" << endl;
		return this;
	}

	const Date* operator&()const
	{
		cout << "const Date* operator&()const" << endl;
		return this;
	}
private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};

int main()
{
	Date d1;
	const Date d2;

	cout << &d1 << endl;
	cout << &d2 << endl;
	
	return 0;
}