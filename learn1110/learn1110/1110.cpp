#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
using namespace std;

//typedef struct SeqList
//{
//	int a[100];
//	size_t size;
//}SL;
//
//void SeqModify(SL* psl, int pos, int x)
//{
//	assert(pos >= 0 && pos < 100);
//
//	psl->a[pos] = x;
//}
//
//int SeqGet(SL* psl, int pos)
//{
//	assert(pos >= 0 && pos < 100);
//
//	return psl->a[pos];
//}
//
//int& SeqAt(SL& sl, int pos)
//{
//	assert(pos >= 0 && pos < 100);
//
//	return sl.a[pos];
//}
//
//int main()
//{
//	SL sl;
//
//	//旧版本
//	SeqModify(&sl, 0, 12);
//	int ret = SeqGet(&sl, 0);
//	cout << ret << endl;
//
//	//新版本
//	cout << SeqAt(sl, 0) << endl;
//	SeqAt(sl, 0) = 15;
//	cout << SeqAt(sl, 0) << endl;
//	SeqAt(sl, 0) += 6;
//	cout << SeqAt(sl, 0) << endl;
//
//	return 0;
//}


//int main()
//{
//	// 不可以
//	// 引用过程中，权限不能放大
//	const int a = 0;
//	//int& b = a;
//
//	// 可以，c拷贝给d，没有放大权限，因为d的改变不影响c
//	const int c = 0;
//	int d = c;
//
//	// 可以
//	// 引用过程中，权限可以平移或者缩小
//	int x = 0;
//	int& y = x;
//	const int& z = x;
//	++x;
//	//++z;
//
//	const int& m = 10;
//
//	double dd = 1.11;
//	int ii = dd;
//
//	const int& rii = dd;
//
//	return 0;
//}

//int func1()
//{
//	static int x = 0;
//	return x;
//}

//int& func2()
//{
//	static int x = 0;
//	return x;
//}
//
//int main()
//{
//	//int& ret1 = func1();  // 权限放大，不可以
//	const int& ret1 = func1(); // 权限平移
//	int rret1 = func1();  // 拷贝
//
//	int& ret2 = func2();		// 权限平移
//	const int& rret2 = func2();  // 权限缩小
//
//	return 0;
//}

//#define Add(x, y) ((x)+ (y))
inline int Add(int x, int y)
{
	return x + y;
}

int main()
{
	for (int i = 0; i < 1000; i++)
	{
		cout << Add(i, i + 1) << endl;
	}
	return 0;
}


