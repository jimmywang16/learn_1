#define _CRT_SECURE_NO_WARNINGS 1

#include "contact.h"

//未修改静态版本
//void Initcon(Contact* con)
//{
//	con->sz = 0;
//	memset(con->data, 0, sizeof(con->data));
//}

void Initcon(Contact* con)
{
	con->sz = 0;
	Peoinfo* ptr = (Peoinfo*)calloc(Default, sizeof(Peoinfo));
	if (ptr == NULL)
	{
		perror("Initcon::calloc\n");
	}
	con->data = ptr;
	con->capacity = Default;
}

void Destroycon(Contact* con)
{
	free(con->data);
	con->data = NULL;
	con->sz = 0;
	con->capacity = 0;
	con = NULL;
}



void checkcapa(Contact* con)
{
	if (con->sz == con->capacity)
	{
		Peoinfo* ptr = (Peoinfo*)realloc(con->data, sizeof(Peoinfo)*(con->capacity+Readd));
		if (ptr == NULL)
		{
			perror("checkcapa::realloc\n");
		}
		con->data = ptr;
		con->capacity += Readd;
		printf("添加成功\n");
	}
}
void Addcontact(Contact* con)
{
	checkcapa(con);
//	if (con->sz == 100)
//	{
//		printf("通讯录已满，不能添加\n");
//		return;
//	}

	printf("添加联系人");
	printf("请输入名字");
	scanf("%s", con->data[con->sz].name);
	printf("请输入年龄");
	scanf("%d", &(con->data[con->sz].age));
	printf("请输入性别");
	scanf("%s", con->data[con->sz].sex);
	printf("请输入地址");
	scanf("%d", con->data[con->sz].addr);
	printf("请输入电话");
	scanf("%d", con->data[con->sz].tele);

	con->sz++;
}

void Showcontact(Contact* con)
{
	printf("名字\t年龄\t性别\t地址\t电话\n");

	int i = 0;
	for (i = 0; i < con->sz; i++)
	{
		printf("%s\t%d\t%s\t%s\t%s\n", con->data[i].name,
			con->data[i].age,
			con->data[i].sex,
			con->data[i].addr,
			con->data[i].tele);
	}

}

int Findbynames(Contact* con, char names[])
{
	int i = 0;
	//int del = 0;
	for (i = 0; i < con->sz; i++)
	{
		if (strcmp(names, con->data[i].name) == 0)
		{
			//del = i;
			//break;
			return i;
		}
	}
	return -1;
}
void Delcontact(Contact* con)
{
	char names[Namemax];
	scanf("%s", names);
	int ret = Findbynames(con, names);
	if (ret == -1)
	{
		printf("不存在此人\n");
		return;
	}
	//int i = 0;
	//int del = 0;
	//for (i = 0; i < con->sz; i++)
	//{
	//	if (strcmp(names, &(con->data[i])) == 0)
	//	{
	//		del = i;
	//		break;
	//	}
	//}
	int i = 0;
	for (i = ret; i < con->sz - 1; i++)
	{
		con->data[i] = con->data[i + 1];
	}
	con->sz--;
	printf("删除成功\n");
}

void Searchcontact(Contact* con)
{
	char names[Namemax];
	scanf("%s", names);
	int pos = Findbynames(con, names);
	if (pos == -1)
	{
		printf("查无此人\n");
		return;
	}
	printf("名字\t年龄\t性别\t地址\t电话\n");
	printf("%s\t%d\t%s\t%s\t%s\n", con->data[pos].name,
		con->data[pos].age,
		con->data[pos].sex,
		con->data[pos].addr,
		con->data[pos].tele);
	printf("查找成功\n");

}

void Modicontact(Contact* con)
{
	char names[Namemax];
	scanf("%s", names);
	int pos = Findbynames(con, names);
	if (pos == -1)
	{
		printf("查无此人\n");
		return;
	}

	printf("请修改联系人");
	printf("请输入名字");
	scanf("%s", con->data[pos].name);
	printf("请输入年龄");
	scanf("%d", &(con->data[pos].age));
	printf("请输入性别");
	scanf("%s", con->data[pos].sex);
	printf("请输入地址");
	scanf("%d", con->data[pos].addr);
	printf("请输入电话");
	scanf("%d", con->data[pos].tele);

	printf("修改完成\n");
}