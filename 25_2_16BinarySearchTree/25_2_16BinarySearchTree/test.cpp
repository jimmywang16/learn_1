#define _CRT_SECURE_NO_WARNINGS 1
#include"BinarySearchTree.h"

int main()
{
	b::BSTree<int> t;
	t.Insert(1);
	t.Insert(2);
	t.Insert(3);
	t.Insert(4);
	t.Insert(5);

	t.Find(4);

	t.InOrder();

	t.Erase(5);
	t.Erase(1);
	t.InOrder();

	return 0;
}