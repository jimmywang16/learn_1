#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"


void STInit(ST* ps)
{
	assert(ps);

	ps->a = (DataType*)malloc(sizeof(DataType)* 4);
	if (ps->a == NULL)
	{
		perror("malloc fail");
		return;
	}

	ps->top = 0;
	ps->capacity = 4;
}

void STPush(ST* ps, DataType x)
{
	assert(ps);

	if (ps->top == ps->capacity)
	{
		DataType* tmp = (DataType*)realloc(ps->a,sizeof(DataType)* ps->capacity *2);
		if (tmp == NULL)
		{
			perror("realloc error");
			return;
		}
		ps->a = tmp;
		ps->capacity *= 2;
	}
	ps->a[ps->top] = x;
	ps->top++;
}

void STPop(ST* ps)
{
	assert(ps);
	assert(!STEmpty(ps));

	ps->top -= 1;
}
int STSize(ST* ps)
{
	assert(ps);

	return ps->top;
}

bool STEmpty(ST* ps)
{
	assert(ps);

	return ps->top == 0;
}

void STDestroy(ST* ps)
{
	assert(ps);

	free(ps->a);
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}
DataType STTop(ST* ps)
{
	assert(ps);
	assert(!STEmpty(ps));

	return ps->a[ps->top - 1];
}