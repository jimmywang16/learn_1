#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int DataType;
typedef struct QueueNode
{
	struct QueueNode* next;
	DataType data;
}QNode;

typedef struct QueueList
{
	QNode* head;
	QNode* tail;
	DataType size;
}Queue;

void QueueInit(Queue* pq);
void QueueDestroy(Queue* pq);
void QueuePush(Queue* pq, DataType x);
void QueuePop(Queue* pq);
bool QueueEmpty(Queue* pq);
DataType QueueSize(Queue* pq);
DataType QueueFront(Queue* pq);
DataType QueueBack(Queue* pq);
