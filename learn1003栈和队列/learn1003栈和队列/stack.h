#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int DataType;

typedef struct Stack
{
	DataType* a;
	int top;
	int capacity;
}ST;

void STInit(ST* ps);
void STPush(ST* ps, DataType x);
void STPop(ST* ps);
int STSize(ST* ps);
bool STEmpty(ST* ps);
void STDestroy(ST* ps);
DataType STTop(ST* ps);