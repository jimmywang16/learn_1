#define _CRT_SECURE_NO_WARNINGS 1
#include"queue.h"

int main()
{
	Queue que;
	QueueInit(&que);
	QueuePush(&que, 1);
	QueuePush(&que, 2);
	QueuePush(&que, 3);
	QueuePush(&que, 4);
	QueuePush(&que, 5);
	QueuePush(&que, 6);

	while (!QueueEmpty(&que))
	{
		printf("%d ", QueueFront(&que));

		QueuePop(&que);
	}
	printf("\n");

	QueueDestroy(&que);
	return 0;
}