#define _CRT_SECURE_NO_WARNINGS 1
#include "vector.h"

void test1()
{

	a::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	a::vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test2()
{
	a::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);

	for (auto v1 : v)
	{
		cout << v1 << " ";
	}
	cout << endl;

	a::vector<int>::iterator it = v.begin();
	for (int i = 0; i < v.size(); i++)
	{
		v[i]++;
	}

	for (auto v1 : v)
	{
		cout << v1 << " ";
	}
	cout << endl;

	v.insert(v.begin() + 2, 8);
	for (auto v1 : v)
	{
		cout << v1 << " ";
	}
	cout << endl;

	v.erase(v.begin());
	v.erase(v.begin());
	v.erase(v.begin()); 
	for (auto v1 : v)
	{
		cout << v1 << " ";
	}
	cout << endl;
}
void test3()
{
	a::vector<int> v;
	v.resize(10);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	a::vector<int> v4(v);
	for (auto e : v4)
	{
		cout << e << " ";
	}
	cout << endl;
	v4.push_back(6);
	for (auto e : v4)
	{
		cout << e << " ";
	}
	cout << endl;

	a::vector<int> v5;
	v5 = v4;
	for (auto e : v4)
	{
		cout << e << " ";
	}
}
void test4()
{
	a::vector<string> v;
	v.push_back("111111");
	v.push_back("222222");
	v.push_back("333333");
	v.push_back("444444");
	v.push_back("555555");
	v.push_back("666666");
	for (auto e : v)
	{
		cout << e << endl;
	}

	cout << endl;

	a::vector<string> v1(v);
	for (auto e : v1)
	{
		cout << e << endl;
	}
	cout << endl;

	a::vector<string> v2(v1.begin(),v1.end());
	for (auto e : v2)
	{
		cout << e << endl;
	}
	cout << endl;

	//a::vector<int> v3(10u, 5);
	a::vector<int> v3(10, 5);
	for (auto e : v3)
	{
		cout << e << endl;
	}
}
int main()
{
	test4();
	return 0;
}