#define _CRT_SECURE_NO_WARNINGS 1
#include"AVLTree.h"

int main()
{
	b::AVLTree<int, int> t;
	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	
	for (auto e : a)
	{
		t.Insert(make_pair(e, e));
	}
	t.Inorder();
	return 0;
}