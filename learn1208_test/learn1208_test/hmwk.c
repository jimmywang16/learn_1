#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void func(int* p, int arr[][3])
{
	int i = 0, j = 0;
	int a = 0, b = 0, c = 0, d = 0;
	int max = arr[0][0];
	int min = arr[0][0];
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (*(p + i*3 +j) > max)
			{
				max = *(p + i * 3 + j);
				a = i;
				b = j;
			}

			if (*(p + i * 3 + j) < min)
			{
				min = *(p + i * 3 + j);
				c = i;
				d = j;
			}
		}
	}
	printf("max = %d, 行为%d, 列为%d\n", max, a, b);
	printf("min = %d, 行为%d, 列为%d\n", min, c, d);
}
int main()
{
	int arr[3][3] = { {1, 2, 3}, {45, 56, 12}, {-8, 33, 99} };
	
	int max = arr[0][0];
	int min = arr[0][0];
	int *p = &arr[0][0];
	func(p, arr);
	return 0;
}
//int main()
//{
//	int arr[3][3] = { {1, 2, 3}, {45, 56, 12}, {-8, 33, 99} };
//	int i = 0, j = 0;
//	int a = 0, b = 0, c = 0, d = 0;
//	int max = arr[0][0];
//	int min = arr[0][0];
//	int (*p)[3] = arr;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			if (*(*(p + i) + j) > max)
//			{
//				max = *(*(p + i) + j);
//				a = i;
//				b = j;
//			}
//			
//			if (*(*(p + i) + j) < min)
//			{
//				min = *(*(p + i) + j);
//				c = i;
//				d = j;
//			}
//		}
//	}
//	printf("max = %d, 行为%d, 列为%d\n", max, a, b);
//	printf("min = %d, 行为%d, 列为%d\n", min, c, d);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[3][5] =
//	{ 
//		{34,2,37,35,66},
//		{54,39,24,65,4},
//		{55,3,-234,12,45}
//	};
//	int x = 0, y = 0, a = 0, b = 0;
//	int (*p)[5] = arr;
//	int max = arr[0][0];
//	int min = arr[0][0];
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			if (*(*(p + i) + j) > max)
//			{
//				max = *(*(p + i) + j);
//				x = i;
//				y = j;
//			}
//
//			if (*(*(p + i) + j) < min)
//			{
//				min = *(*(p + i) + j);
//				a = i;
//				b = j;
//			}
//		}
//	}
//	printf("%d   %d\n", max, min);
//	printf("%d   %d\n", x + 1, y + 1);
//	printf("%d   %d\n", a + 1, b + 1);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[3][5] =
//	{
//		{34,2,37,35,66},
//		{54,39,24,65,4},
//		{55,3,-234,12,45}
//	};
//	int x = 0, y = 0, a = 0, b = 0;
//	int (*p)[5] = arr;
//	int max = arr[0][0];
//	int min = arr[0][0];
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			if (*(*(p + i) + j) > max)
//			{
//				max = *(*(p + i) + j);
//				x = i;
//				y = j;
//			}
//			else//(*(*(p + i) + j) < min)
//			{
//				min = *(*(p + i) + j);
//				a = i;
//				b = j;
//			}
//		}
//	}
//	printf("%d   %d\n", max, min);
//	printf("%d   %d\n", x + 1, y + 1);
//	printf("%d   %d\n", a + 1, b + 1);
//	return 0;
//}