#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>

#define MAX 100
#define Namemax 20
#define Sexmax 20
#define Addrmax 20
#define Telemax 20


typedef struct Peoinfo
{
	char name[Namemax];
	int age;
	char sex[Sexmax];
	char addr[Addrmax];
	char tele[Telemax];
}Peoinfo;

typedef struct Contact
{
	Peoinfo data[MAX];
	int sz;
}Contact;

void Initcon(Contact* con);

void Addcontact(Contact* con);

void Showcontact(Contact* con);

void Delcontact(Contact* con);

void Searchcontact(Contact* con);

void Modicontact(Contact* con);

//ֻʣqsort�����