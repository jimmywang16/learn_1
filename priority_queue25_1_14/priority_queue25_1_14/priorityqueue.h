#pragma once

template<class T>
class Less
{
public:
	bool operator()(const T& x, const T& y)
	{
		return x < y;
	}

};
template<class T>
class Greater
{
public:
	bool operator()(const T& x, const T& y)
	{
		return x > y;
	}
};

namespace a
{
	template<class T, class Container = vector<T>, class Compare = Less<T>>
	class priority_queue
	{
	private:
		void Adjustdown(int parent)
		{
			Compare com;
			int child = 2 * parent + 1;
			while (child < _con.size())
			{
				
				if(child + 1 < _con.size() && com(_con[child], _con[child + 1]))
				//if (child + 1 < _con.size() && _con[child] < _con[child + 1])
				{
					++child;
				}
				//if (_con[parent] < _con[child])
				if(com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = 2 * parent + 1;
				}
				else
				{
					break;
				}
			}
		}

		void Adjustup(int child)
		{
			Compare com;
			int parent = (child - 1) / 2;
			while (child > 0)
			{
				if(com(_con[parent], _con[child]))
				//if (_con[parent] < _con[child])
				{
					swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
	public:
		priority_queue()
		{}
		
		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				_con.push_back(*first);
				first++;
			}

			//����
			for (int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
			{
				Adjustdown(i);
			}
		}

		void pop()
		{
			swap(_con[0], _con[_con.size()-1]);
			_con.pop_back();
			Adjustdown(0);
		}

		void push(const T& x)
		{
			_con.push_back(x);
			Adjustup(_con.size() - 1);
		}

		const T& top()
		{
			return _con[0];
		}

		size_t size()const
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};

	void test1()
	{
		vector<int> v;
		v.push_back(10);
		v.push_back(1);
		v.push_back(12);
		v.push_back(14);
		v.push_back(7);

		priority_queue<int> pq(v.begin(), v.end());
		

		while (!pq.empty())
		{
			cout << pq.top() << " ";
			pq.pop();
		}
		cout << endl;
		
		priority_queue<int, vector<int>, Greater<int>> pq1;
		pq1.push(1);
		pq1.push(10);
		pq1.push(20);
		pq1.push(45);
		pq1.push(16);
		pq1.push(9);
		pq1.push(32);

		cout << pq1.size() << endl;

		while (!pq1.empty())
		{
			cout << pq1.top() << " ";
			pq1.pop();
		}


	}
}
