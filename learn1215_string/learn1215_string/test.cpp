#define _CRT_SECURE_NO_WARNINGS 1
#include "string.h"

void test1()
{
	myspace::string s1("Yes my Lord");
	cout << s1.c_str() << endl;
	cout << s1.size() << endl;

	myspace::string s2;
	cout << s2.c_str() << endl;
	cout << s2.size() << endl;
	cout << s1[4] << endl;

	s1.push_back('_');
	s1.push_back('_');
	s1.push_back('_');
	s1.append("Mr.W");
	cout << s1.c_str() << endl;
	
	myspace::string s3("Your Majesty");
	cout << s3.c_str() << endl;

	/*myspace::string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}*/
}
void test2()
{
	myspace::string s1("123456");
	cout << s1.c_str() << endl;
}

int main()
{
	test2();
	//test2();
	return 0;
}