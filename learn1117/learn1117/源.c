#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
#include<stdio.h>
//using namespace std;

//class Date
//{
//public:
//	//// 1.无参构造函数
//	//Date()
//	//{}
//
//	// 2.带参构造函数
//	Date(int year, int month)
//		:_year(year)
//		//,_month(month)
//		//,_day(day)
//	{}
//
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//void TestDate()
//{
//	Date d1(2024,5); // 调用无参构造函数
//	d1.Print();
//	//Date d2(2015, 1, 1); // 调用带参的构造函数
//
//}
//
//class A
//{
//public:
//	A(int a)
//		:_a(a)
//
//	{}
//private:
//	int _a;
//};
//void Test2()
//{
//	A a(3);
//	A b = 2; //隐式类型转换，整形转换成自定义类型
//	// 2构造一个A的临时对象，临时对象再拷贝构造b -->优化用2直接构造
//}
//
//int main()
//{
//	Test2();
//	return 0;
//}

//class Sum
//{
//public:
//    Sum()
//    {
//        _ret += _i;
//        ++_i;
//    }
//
//    static int GetRet()
//    {
//        return _ret;
//    }
//private:
//    static int _i;
//    static int _ret;
//};
//
//int Sum::_i = 1;
//int Sum::_ret = 0;
//
//class Solution {
//public:
//    int Sum_Solution(int n) {
//    Sum a[n];
//
//    return Sum::GetRet();
//    }
//};

//class A
//{
//public:
//    A(int a = 0)
//        :_a(a)
//    {
//        cout << "A(int a)" << endl;
//    }
//
//    ~A()
//    {
//        cout << "~A()" << endl;
//    }
//private:
//    int _a;
//};
//
//class Solution {
//public:
//    int Sum_Solution(int n) {
//        cout << "Sum_Solution" << endl;
//        //...
//        return n;
//    }
//};

//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//	A& operator=(const A& aa)
//	{
//		cout << "A& operator=(const A& aa)" << endl;
//		if (this != &aa)
//		{
//			_a = aa._a;
//		}
//		return *this;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
int main()
{
	char arr1[] = "bit";
	char arr2[] = { 'b', 'i', 't' };
	char arr3[] = { 'b', 'i', 't', '\0' };
	printf("%d\n", sizeof(arr1));
	printf("%d\n", sizeof(arr2));
	printf("%d\n", sizeof(arr3));
	return 0;
}