#define _CRT_SECURE_NO_WARNINGS 1
#include"RBTree.h"
#include"Set.h"
#include"Map.h"
#include<string>

int main()
{
	a::map<int, int> m;
	m.insert(make_pair(1, 1));
	m.insert(make_pair(3, 3));
	m.insert(make_pair(2, 2));

	//bit::map<int, int>::iterator mit = m.begin();
	auto mit = m.begin();
	while (mit != m.end())
	{
		// 不能修改key，可以修改value
		//mit->first = 1;
		mit->second = 2;

		cout << mit->first << ":" << mit->second << endl;
		++mit;
	}
	cout << endl;


	for (const auto& kv : m)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;

	a::set<int> s;
	s.insert(5);
	s.insert(2);
	s.insert(2);
	s.insert(12);
	s.insert(22);
	s.insert(332);
	s.insert(7);
	a::set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		// 不应该允许修改key
	/*	if (*it % 2 == 0)
		{
			*it += 10;
		}*/

		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (const auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;


	return 0;
}