#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int DataType;
typedef struct Heap
{
	DataType* a;
	int size;
	int capacity;
}HP;

void HeapInit(HP* php);
void HeapDestroy(HP* php);

void HeapPush(HP* php, DataType x);
void HeapPop(HP* php);

bool HeapEmpty(HP* php);
DataType HeapTop(HP* php);
int HeapSize(HP* php);

void AdjustUp(DataType* a, int child);
void AdjusDown(DataType* a, int n, int parent);

void Swap(DataType* p1, DataType* p2);

void HPInitArray(HP* php, DataType* a, int n);