#define _CRT_SECURE_NO_WARNINGS 1
#include"heap.h"

void HeapInit(HP* php)
{
	assert(php);

	php->a = (DataType*)malloc(sizeof(DataType)*4);
	if (php->a == NULL)
	{
		perror("malloc fail");
		return;
	}

	php->capacity = 4;
	php->size = 0;

}
void HeapDestroy(HP* php)
{
	assert(php);

	free(php->a);
	php->a = NULL;
	php->capacity = 0;
	php->size = 0;
}

void Swap(DataType* p1, DataType* p2)
{
	DataType x = *p1;
	*p1 = *p2;
	*p2 = x;
}
void AdjustUp(DataType* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjusDown(DataType* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		//判断左右大小根堆
		if (child + 1 < n && a[child + 1] > a[child])
		{
			child++;
		}

		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapPush(HP* php, DataType x)
{
	assert(php);

	if (php->size == php->capacity)
	{
		DataType* tmp = (DataType*)realloc(php->a,sizeof(DataType)* php->capacity *2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}

		php->a = tmp;
		php->capacity *= 2;
	}

	php->a[php->size] = x;
	php->size++;

	AdjustUp(php->a, php->size -1);
}
void HeapPop(HP* php)
{
	assert(php);
	assert(!HeapEmpty(php));

	//删除数据
	Swap(&php->a[0], &php->a[php->size -1]);
	php->size--;

	AdjusDown(php->a, php->size , 0);
}

bool HeapEmpty(HP* php)
{
	assert(php);

	return php->size == 0;
}

DataType HeapTop(HP* php)
{
	assert(php);

	return php->a[0];
}

int HeapSize(HP* php)
{
	assert(php);

	return php->size;
}

void HPInitArray(HP* php, DataType* a, int n)
{
	assert(php);

	php->a = (DataType*)malloc(sizeof(DataType)* n);
	if (php->a == NULL)
	{
		perror("malloc fail");
		return;
	}

	php->capacity = n;
	php->size = n;

	//向下建堆

	for (int i = (n-1-1)/2; i >= 0; i--)
	{
		AdjusDown(php->a, php->size, i);
	}
}