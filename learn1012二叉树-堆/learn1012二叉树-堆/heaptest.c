#define _CRT_SECURE_NO_WARNINGS 1
#include"heap.h"

//int main()
//{
//	HP hp;
//	HeapInit(&hp);
//	HeapPush(&hp, 4);
//	HeapPush(&hp, 18);
//	HeapPush(&hp, 42);
//	HeapPush(&hp, 12);
//	HeapPush(&hp, 21);
//	HeapPush(&hp, 3);
//	HeapPush(&hp, 5);
//	HeapPush(&hp, 5);
//	HeapPush(&hp, 50);
//	HeapPush(&hp, 5);
//	HeapPush(&hp, 5);
//	HeapPush(&hp, 15);
//	HeapPush(&hp, 5);
//	HeapPush(&hp, 45);
//	HeapPush(&hp, 5);
//
//	int k = 0;
//	scanf("%d", &k);
//	while (!HeapEmpty(&hp) && k--)
//	{
//		printf("%d ", HeapTop(&hp));
//		HeapPop(&hp);
//	}
//	printf("\n");
//
//	return 0;
//}

//int main()
//{
//	HP hp;
//	HeapInit(&hp);
//	HeapPush(&hp, 9);
//	HeapPush(&hp, 10);
//	HeapPush(&hp, 9988);
//	HeapPush(&hp, 88);
//	HeapPush(&hp, 69);
//	HeapPush(&hp, 39);
//	HeapPush(&hp, 49);
//	HeapPush(&hp, 95);
//	HeapPush(&hp, 91);
//
//	int k = 0;
//
//	scanf("%d", &k);
//	while (!HeapEmpty(&hp) && k--)
//	{
//		printf("%d ", HeapTop(&hp));
//		HeapPop(&hp);
//	}
//	HeapDestroy(&hp);
//	printf("\n");
//	return 0;
//}

void HeapSort(int* a, int n)
{
	////建堆--向上建堆
	//for (int i = 1; i < n; i++)
	//{
	//	AdjustUp(a, i);
	//}

	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjusDown(a, end, 0);

		end--;
	}

	////建堆--向下调整建堆
	//for (int j = (n - 1 - 1) / 2; j >= 0; j--)
	//{
	//	AdjusDown(a, n, j);
	//}
}

int main()
{
	int a[10] = {10,8,9,7,4,5,6,1,2,3};
	HeapSort(a, 10);
	return 0;
}