#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void test2()
{
	char arr1[] = "hello\0";
	char arr2[] = "hello";
	char arr3[] = { 'h','e','l','l','o' };

	int sz1 = sizeof(arr1);
	int sz2 = sizeof(arr2);
	int sz3 = sizeof(arr3);

	printf("%d", sz1);
	printf("%d", sz2);
	printf("%d", sz3);
}


int main()
{
	//Test();
	char arr1[] = "hello";
	char arr2[] = "hello";
	char arr3[] = { 'h','e','l','l','o' };

	int sz1 = sizeof(arr1);
	int sz2 = sizeof(arr2);
	int sz3 = sizeof(arr3);

	printf("%d", sz1);
	printf("%d", sz2);
	printf("%d", sz3);
	return 0;
}