#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_STU 100
#define MAX_BOOK 10
#define MAX_DAY 90

typedef struct Student
{
	int _id;
	char _name[32];
	char _class[32];
	int _bookcount;
	struct book
	{
		char _name[32];
		int _days;
	}_book[MAX_BOOK];

}Student;

Student* findStudent_id(Student* students, int studentcount, int id)
{
	for (int i = 0; i < studentcount; ++i)
	{
		if (students[i]._id == id)
		{
			return &students[i];
		}

	}
	return NULL;
}

void signup(Student* students, int* studentcount)
{
	//判断当前是否达到最大值
	if (*studentcount >= MAX_STU)
	{
		printf("已经达到最大数量上限，无法注册\n");
		return;
	}
	Student* newstudent = &students[*studentcount];

	printf("输入学生信息：\n");
	//1
	printf("学号->");
	while (1)
	{
		int id;
		scanf("%d", &id);
		getchar();

		if (findStudent_id(students, *studentcount, id) == NULL)
		{
			newstudent->_id = id;
			break;
		}
		else
		{
			printf("学号已经存在，请重新输入->");
		}
	}
	//2
	printf("姓名->");
	gets(newstudent->_name);
	//3
	printf("班级->");
	gets(newstudent->_class);
	//4
	printf("图书数量->");
	while (1)
	{
		int bookcount = 0;
		scanf("%d", &bookcount);
		getchar();

		if (bookcount > 0 && bookcount <= MAX_BOOK)
		{
			newstudent->_bookcount = bookcount;
			break;
		}
		else
		{
			printf("学科数量不合法，请重新输入->");
		}
	}

	//5
	printf("请输入借阅图书信息:\n");
	for (int i = 0; i < newstudent->_bookcount; ++i)
	{
		printf("请输入第%d本图书信息：\n", i + 1);

		printf("图书名称");
		gets(newstudent->_book[i]._name);

		//输入days
		printf("借阅天数->");
		while (1)
		{
			int days;
			scanf("%d", &days);
			getchar();

			if (days >= 0 && days <= MAX_DAY)
			{
				newstudent->_book[i]._days = days;
				break;
			}
			else
			{
				printf("\n借阅天数不合规，重新输入->");
			}
		}
	}

	++(*studentcount);
}

void deleteStudent(Student* students, int* studentcount)
{
	if (*studentcount == 0)
	{
		printf("还没有学生信息！\n");
		return;
	}

	printf("请输入删除的学生学号->");
	int id;
	scanf("%d", &id);
	int found = 0;
	for (int i = 0; i < *studentcount; ++i)
	{
		if (students[i]._id == id)
		{
			for (int j = 0; j < *studentcount - 1; ++j)
			{
				students[j] = students[j + 1];

			}
			--(*studentcount);
			found = 1;
			break;
		}
	}
	if (found == 0)
	{
		printf("没找到该学生信息!!!\n");
	}

}

void modifyStudent(Student* students, int* studentcount)
{
	if (*studentcount == 0)
	{
		printf("没找到该学生信息!!!\n");
		return;
	}
	printf("请输入要修改的学生学号->");
	int id = 0;
	scanf("%d", &id);
	int found = 0;

	for (int i = 0; i < *studentcount; ++i)
	{
		if (students[i]._id == id)
		{
			printf("请输入学生信息：\n");
			getchar();

			printf("请输入学生姓名->");
			gets(students[i]._name);

			printf("请输入学生班级->");
			gets(students[i]._class);

			printf("请输入图书数量->");

			while (1)
			{
				int bookcount = 0;
				scanf("%d", &bookcount);
				getchar();

				if (bookcount > 0 && bookcount <= MAX_BOOK)
				{
					students[i]._bookcount = bookcount;
					break;
				}
				else
				{
					printf("图书数量不合法，请重新输入->");
				}
			}

			printf("请输入图书信息:\n");
			for (int j = 0; j < students[i]._bookcount; ++j)
			{
				printf("请输入第%d本图书信息：\n", j + 1);

				printf("图书名称");
				gets(students[i]._book[j]._name);

				//输入days
				printf("天数->");
				while (1)
				{
					int days;
					scanf("%d", &days);
					getchar();

					if (days >= 0 && days <= MAX_DAY)
					{
						students[i]._book[j]._days = days;
						break;
					}
					else
					{
						printf("\n图书天数不合法，重新输入->");
					}
				}
			}
			found = 1;
			break;
		}
	}
	if (found == 0)
	{
		printf("没找到该学生信息!!!\n");
	}

}

void findStudent_info(Student* students, int* studentcount)
{
	if (*studentcount == 0)
	{
		printf("无学生信息，查询失败\n");
		return;
	}

	printf("请输入查询的学生学号->");
	int id;
	scanf("%d", &id);
	int found = 0;

	for (int i = 0; i < *studentcount; ++i)
	{
		if (students[i]._id == id)
		{
			printf("学生信息如下：");
			printf("学号:%d\n", students[i]._id);
			printf("姓名:%s\n", students[i]._name);
			printf("班级:%s\n", students[i]._class);
			printf("图书数量:%d\n", students[i]._bookcount);

			for (int j = 0; j < students[i]._bookcount; ++j)
			{
				printf("第%d本图书：\n", j + 1);
				printf("图书名称:%s\n", students[i]._book[j]._name);
				printf("借阅天数:%d\n", students[i]._book[j]._days);
			}
			found = 1;
			break;
		}
	}
	if (found == 0)
	{
		printf("没有找到该学生信息！\n");
	}
}

int login()
{
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********图书管理系统登录********\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");

	char user[20];
	char password[20];

	printf("请输入登陆账户:\n");
	gets(user);

	printf("请输入密码:\n");
	gets(password);

	if (strcmp(user, "admin") == 0 && strcmp(password, "123456") == 0)
	{
		printf("登陆成功\n\n\n\n");
		return 1;
	}
	else
	{
		printf("登录失败\n");
		return 0;
	}
}

void menu()
{
	printf("\n\n\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*******图书馆图书管理菜单********\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("*********************************\n");
	printf("#################################\n");
	printf("#################################\n");
	printf("#################################\n");
	printf("######1、注册/添加学生信息#######\n");
	printf("########2、删除学生信息##########\n");
	printf("########3、修改学生信息##########\n");
	printf("########4、查询学生信息##########\n");
	printf("###########5、exit###############\n");
	printf("#################################\n");
	printf("#################################\n");
	printf("#################################\n");

}

void main_menu(Student* students, int* studentcount)
{
	int choice;

	do
	{
		menu();
		printf("请输入您的选择：\n");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1:
			printf("这是注册功能\n");
			signup(students, studentcount);
			break;
		case 2:
			deleteStudent(students, studentcount);
			printf("这是删除功能\n");
			break;
		case 3:
			printf("这是修改功能\n");
			modifyStudent(students, studentcount);
			break;
		case 4:
			printf("这是查询功能\n");
			findStudent_info(students, studentcount);
			break;
		case 5:

			break;
		default:
			printf("输入错误，请重新输入\n");
		}

		if (choice == 5)
		{
			printf("exit successfully\n");
			break;
		}


	} while (choice != 16);
}

void sys_info()
{
	printf("此图书馆管理系统包含：\n");
	printf("\n");
	printf("1->学生id\n");
	printf("2->学生名\n");
	printf("3->班级\n");
	printf("4->借阅图书数量\n");
	printf("5->借阅图书：(1)名称 (2)天数\n");
	printf("注：借阅图书数量不得超过10本！！！！！\n");
	printf("注：借阅图书天数不得超过90天！！！！！\n");
	printf("\n\n\n");
}


int main()
{
	sys_info();
	Student students[MAX_STU];
	int studentcount = 0;

	if (!login())
	{
		printf("登录失败，退出系统\n");
		return 1;
	}


	main_menu(students, &studentcount);

	return 0;
}