#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>

using namespace std;

//int main(void)
//{
//	vector<int>array;
//	array.push_back(100);
//	array.push_back(300);
//	array.push_back(300);
//	array.push_back(300);
//	array.push_back(300);
//	array.push_back(500);
//	vector<int>::iterator itor;
//
//	for (itor = array.begin(); itor != array.end(); itor++)
//	{
//		if (*itor == 300)
//		{
//			itor = array.erase(itor);
//		}
//
//	}
//
//	for (itor = array.begin(); itor != array.end(); itor++)
//	{
//		cout << *itor << " ";
//	}
//
//	return 0;
//
//}
//int main()
//{
//	int ar[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int n = sizeof(ar) / sizeof(int);
//	vector<int> v(ar, ar + n);
//	cout << v.size() << ":" << v.capacity() << endl;
//	v.reserve(100);
//	v.resize(20);
//	cout << v.size() << ":" << v.capacity() << endl;
//
//	v.reserve(50);
//
//	v.resize(5);
//	cout << v.size() << ":" << v.capacity() << endl;
//	return 0;
//}
//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int slow = 0;
//        int fast = 1;
//        vector<int> v;
//        for (slow = 0; slow < nums.size(); slow++) {
//
//            for (fast = 1; fast < nums.size(); fast++) {
//                if (nums[slow] == nums[fast]) {
//                    break;
//                }
//
//            }
//            if (fast == nums.size()) {
//                v.push_back(nums[slow]);
//            }
//
//        }
//        return v;
//    }
//};
class Solution {
public:

    int MoreThanHalfNum_Solution(vector<int>& nums) {
        auto slow = nums.begin();
        auto fast = nums.end();
        int flag = 0;
        for (slow = nums.begin(); slow < nums.end(); slow++) {
            flag = 0;
            for (fast = nums.begin(); fast < nums.end(); fast++) {
                if (*fast == *slow) {
                    flag++;
                }
            }
            if (flag > nums.size() / 2) {
                return *slow;
            }
        }
        return 0;
    }
};
int main()
{
    Solution s;
    int arr[] = {1, 2, 2, 2, 2};
    vector<int> v(arr, arr+5);

    int a = s.MoreThanHalfNum_Solution(v);
    cout << a << endl;
    return 0;
}
