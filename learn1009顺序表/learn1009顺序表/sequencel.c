#define _CRT_SECURE_NO_WARNINGS 1
#include"sequencel.h"
void SeqListInit(SeqList* psl, size_t capacity)
{
	assert(psl);

	psl->array = (SLDataType*)malloc(sizeof(SeqList)* capacity);
	if (psl->array == NULL)
	{
		perror("malloc");
		return;
	}
	psl->size = 0;
	psl->capacity = capacity;
}

void SeqListDestory(SeqList* psl)
{
	assert(psl);

	free(psl->array);
	psl->array = NULL;
	psl->capacity = 0;
	psl->size = 0;
}

void CheckCapacity(SeqList* psl)
{
	assert(psl);

	if (psl->size == psl->capacity)
	{
		SLDataType* tmp = (SLDataType*)realloc(psl->array, sizeof(SLDataType)* psl->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc");
			return;
		}
		psl->array = tmp;
		psl->capacity *= 2;
	}
}

//void SeqListPushBack(SeqList* psl, SLDataType x)
//{
//	CheckCapacity(psl);
//	psl->array[psl->size++] = x;
//}
void SeqListPushBack(SeqList* psl, SLDataType x)
{
	assert(psl);

	SeqListInsert(psl, psl->size, x);
}

void SeqListPrint(SeqList* psl)
{
	assert(psl);

	for (size_t i = 0; i < psl->size; i++) printf("%d ", psl->array[i]);
	printf("\n");
}

//void SeqListPopBack(SeqList* psl)
//{
//	assert(psl->size);
//
//	psl->size--;
//}
void SeqListPopBack(SeqList* psl)
{
	assert(psl);

	SeqListErase(psl, psl->size - 1);
}

//void SeqListPushFront(SeqList* psl, SLDataType x)
//{
//	CheckCapacity(psl);
//
//	int tmp = psl->size;
//	while (tmp > 0) //当本身为空时，就不走这个循环
//	{
//		psl->array[tmp] = psl->array[tmp - 1];
//		tmp--;
//	}
//
//	psl->array[0] = x;
//	psl->size++;
//}
void SeqListPushFront(SeqList* psl, SLDataType x)
{
	SeqListInsert(psl, 0, x);
}

//void SeqListPopFront(SeqList* psl)
//{
//	assert(psl->size);
//
//	size_t cur = 0;
//	while (cur < psl->size - 1)
//	{
//		psl->array[cur] = psl->array[cur + 1];
//		cur++;
//	}
//
//	psl->size--;
//}
void SeqListPopFront(SeqList* psl)
{
	SeqListErase(psl, 0);
}

void SeqListInsert(SeqList* psl, size_t pos, SLDataType x)
{
	assert(psl);
	assert(pos >= 0 && pos <= psl->size);

	CheckCapacity(psl);

	size_t cur = psl->size;
	while (cur != pos)
	{
		psl->array[cur] = psl->array[cur - 1];
		cur--;
	}

	psl->array[pos] = x;
	psl->size++;
}

void SeqListErase(SeqList* psl, size_t pos)
{
	assert(psl);
	assert(pos >= 0 && pos < psl->size);

	size_t cur = pos;
	while (cur < psl->size - 1)
	{
		psl->array[cur] = psl->array[cur + 1];
		cur++;
	}

	psl->size--;
}

int SeqListFind(SeqList* psl, SLDataType x)
{
	assert(psl);

	for (size_t i = 0; i < psl->size; i++)
	{
		if (psl->array[i] == x) return i;
	}

	return -1;
}

void SeqListModify(SeqList* psl, size_t pos, SLDataType x)
{
	assert(psl);
	assert(pos >= 0 && pos < psl->size);

	psl->array[pos] = x;
}


