#define _CRT_SECURE_NO_WARNINGS 1
#include"sequencel.h"

void test1()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushBack(&sl, 1);
	SeqListPushBack(&sl, 2);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}


void test2()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushBack(&sl, 1);
	SeqListPushBack(&sl, 2);
	SeqListPrint(&sl);

	SeqListPopBack(&sl);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}


void test3()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushFront(&sl, 1);
	SeqListPushFront(&sl, 2);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}


void test4()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushFront(&sl, 1);
	SeqListPushFront(&sl, 2);
	SeqListPrint(&sl);

	SeqListPopFront(&sl);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}


void test5()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushFront(&sl, 1);
	SeqListPushFront(&sl, 2);
	SeqListPushFront(&sl, 3);
	SeqListPrint(&sl);

	SeqListInsert(&sl, 5, 9);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}


void test6()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushBack(&sl, 1);
	SeqListPushBack(&sl, 2);
	SeqListPushBack(&sl, 3);
	SeqListPrint(&sl);

	SeqListErase(&sl, 2);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}


void test7()
{
	SeqList sl;
	SeqListInit(&sl, 2);

	SeqListPushBack(&sl, 1);
	SeqListPushBack(&sl, 2);
	SeqListPushBack(&sl, 3);
	SeqListPrint(&sl);

	int pos = SeqListFind(&sl, 3);
	SeqListModify(&sl, pos, 9);
	SeqListPrint(&sl);

	SeqListDestory(&sl);
}

int main()
{
	//test1();
	//test2();
	//test3();
	//test4();
	//test5();
	//test6();
	test7();

	return 0;
}