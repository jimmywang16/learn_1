#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define max_element 10

void insert_toarr(int* arr, int* pcount, int index, int num)
{
	if (*pcount >= max_element)
	{
		printf("元素已满\n");
		return;
	}

	arr[index] = num;
	++*pcount;
}

void display(int* arr, int* pcount)
{
	for (int i = 0; i < *pcount; ++i)
	{
		printf("arr[%d]->第%d个元素->%d\n", i, i+1,arr[i]);
	}
	printf("\n");
}

void deletearr(int* arr, int* pcount, int index)
{
	if (*pcount == 0)
	{
		printf("error:element num is 0\n");
		return;
	}

	for (int i = index; i < *pcount - 1; ++i)
	{
		arr[i] = arr[i + 1];
	}

	--*pcount;
}

int main()
{
	int arr[max_element] = { 0 };
	int count = 0;

	int num1 = 1;
	int num2 = 2;
	int num3 = 3;

	printf("******数组操作******\n");
	insert_toarr(arr, &count, 0, num1);
	insert_toarr(arr, &count, 1, num2);
	insert_toarr(arr, &count, 2, num3);

	display(arr, &count);

	deletearr(arr, &count, 1);
	display(arr, &count);


	return 0;
}