#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//冒泡排序思想：左右相邻元素，两两比较
//只能排序整数
int main()
{
	int arr[] = {8,9,6,4,2,44,85,23,47,5};
	int sz = sizeof(arr) / sizeof(arr[0]);
	int i = 0;
	//比较趟数
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		//相邻元素，两两比较
		//每趟比完后少一个数
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}

	for (i = 0; i < sz; i++)
	{
		printf("%d	", arr[i]);
	}
	return 0;
}