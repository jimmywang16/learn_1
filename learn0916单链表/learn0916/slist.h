#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLTDataType;
typedef struct SLTNode
{
	SLTDataType data;
	struct SLTNode* next;
}SLTNode;

void SLTPrint(SLTNode* phead);
void SLTPushBack(SLTNode** pphead, SLTDataType x);
void SLTPushFront(SLTNode** pphead, SLTDataType x);

void SLTPopFront(SLTNode** pphead);
void SLTPopBack(SLTNode** pphead);

SLTNode* BuySLTNode(SLTDataType x);

//单链表查找
SLTNode* SListFind(SLTNode* phead, SLTDataType x);

//pos之前插入
void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);

//pos位置删除
void SListErase(SLTNode** pphead, SLTNode* pos);

//
//pos后面插入
void SListInsertAfter(SLTNode* pos, SLTDataType x);

//pos位置后面删除
void SListEraseAfter(SLTNode* pos);

//销毁
void SLTDestroy(SLTNode* phead);