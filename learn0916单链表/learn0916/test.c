#define _CRT_SECURE_NO_WARNINGS 1
#include "slist.h"

TestSlist1()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);

	SLTPrint(plist);
}

TestSlist2()
{
	SLTNode* plist = NULL;
	SLTPushFront(&plist, 1);
	SLTPushFront(&plist, 2);
	SLTPushFront(&plist, 3);
	SLTPushFront(&plist, 4);

	SLTPrint(plist);

	SLTPopBack(&plist);
	SLTPrint(plist);
}

TestSlist3()
{
	SLTNode* plist = NULL;
	SLTPushFront(&plist, 1);
	SLTPushFront(&plist, 2);
	SLTPushFront(&plist, 3);
	SLTPushFront(&plist, 4);

	SLTPrint(plist);

	SLTPopBack(&plist);
	SLTPrint(plist);

	SLTNode* ret = SListFind(plist, 2);
	//�ⲿ�ÿ�
	SListErase(&plist, ret);

	ret = NULL;

	SLTPrint(plist);

	SLTDestroy(plist);
	plist = NULL;
}

int main()
{
	//TestSlist1();
	//TestSlist2();
	TestSlist3();
	return 0;
}
