﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
using namespace std;

//int main()
//{
//    priority_queue<int> a;//大堆
//    priority_queue<int, vector<int>, greater<int> > c;//小堆
//    priority_queue<string> b;
//    for (int i = 0; i < 5; i++)
//    {
//        a.push(i);
//        c.push(i);
//    }
//    while (!a.empty())
//    {
//        cout << a.top() << ' ';
//        a.pop();
//    }
//    cout << endl;
//    while (!c.empty())
//    {
//        cout << c.top() << ' ';
//        c.pop();
//    }
//    cout << endl;
//    b.push("abc");
//    b.push("abcd");
//    b.push("cbd");
//    while (!b.empty())
//    {
//        cout << b.top() << ' ';
//        b.pop();
//    }
//    cout << endl;
//    return 0;
//}

//﻿template<class Type>
//Type Max(const Type& a, const Type& b)
//{
//	cout << "This is Max<Type>" << endl;
//	return a > b ? a : b;
//}

//template<class Type>
//Type Max(const Type& a, const Type& b)
//{
//	cout << "This is Max<Type>" << endl;
//	return a > b ? a : b;
//}
//
//template<>
//int Max<int>(const int& a, const int& b)
//{
//	cout << "This is Max<int>" << endl;
//	return a > b ? a : b;
//}
//
//template<>
//char Max<char>(const char& a, const char& b)
//{
//	cout << "This is Max<char>" << endl;
//	return a > b ? a : b;
//}
//
//int Max(const int& a, const int& b)
//{
//	cout << "This is Max" << endl;
//	return a > b ? a : b;
//}
//
//int main()
//{
//	Max(10, 20);//Max
//	Max(12.34, 23.45);//Max Type
//	Max('A', 'B');//Max<char>
//	Max<int>(20, 30);//Max<int>
//
//	return 0;
//}

//template<class T1, class T2>
//class Data
//{
//public:
//	Data() { cout << "Data<T1, T2>" << endl; }
//private:
//	T1 _d1;
//	T2 _d2;
//};
//
//template <class T1>
//class Data<T1, int>
//{
//public:
//	Data() { cout << "Data<T1, int>" << endl; }
//private:
//	T1 _d1;
//	int _d2;
//};
//
//template <typename T1, typename T2>
//class Data <T1*, T2*>
//{
//public:
//	Data() { cout << "Data<T1*, T2*>" << endl; }
//private:
//	T1 _d1;
//	T2 _d2;
//};
//
//template <typename T1, typename T2>
//class Data <T1&, T2&>
//{
//public:
//	Data(const T1& d1, const T2& d2)
//		: _d1(d1)
//		, _d2(d2)
//	{
//		cout << "Data<T1&, T2&>" << endl;
//	}
//private:
//	const T1& _d1;
//	const T2& _d2;
//};
//
//int main()
//{
//	Data<double, int> d1;//Data<T1, int>
//	Data<int, double> d2;//Data<T1, T2>
//	Data<int*, int*> d3;//Data<T1*, T2*>
//	Data<int&, int&> d4(1, 2);//Data<T1&, T2&>
//
//	return 0;
//}

//﻿class A
//{
//public:
//  void f() { cout << "A::f()" << endl; }
//  int a;
//};

//

class A
{
public:
  A() { cout << "A::A()" << endl; }
  ~A() { cout << "A::~A()" << endl; }
  int a;
};

class B : public A
{
public:
	B() { cout << "B::B()" << endl; }
	~B() { cout << "B::~B()" << endl; }
	int b;
};

void f()
{
	B b;
}

int main()
{
	f();
	return 0;
}