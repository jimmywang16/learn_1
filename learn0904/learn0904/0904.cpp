#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//struct tag
//{
//	//成员列表
//	int a;
//	char arr[10];
//}variable_list;


//struct
//{
//	int a;
//	char b;
//	float c;
//}x;
////因为匿名，所以只能在这里创建变量，
////其他地方创建不了

//接下来我这样写
//struct
//{
//	int a;
//	char b;
//	float c;
//}x;
//
//struct
//{
//	int a;
//	char b;
//	float c;
//}*p;
////请问我能够在下面写 p = &x 吗？

//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void print(struct Stu* ps)
//{
//	printf("name = %s   age = %d\n", (*ps).name, (*ps).age);
//	//使用结构体指针访问指向对象的成员
//	printf("name = %s   age = %d\n", ps->name, ps->age);
//}
//
//int main()
//{
//	struct Stu s = { "zhangsan", 20 };
//	print(&s);//结构体地址传参
//	return 0;
//}


//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void print(struct Stu* ps)
//{
//	printf("name = %s   age = %d\n", (*ps).name, (*ps).age);
//	//使用结构体指针访问指向对象的成员
//	printf("name = %s   age = %d\n", ps->name, ps->age);
//
//}
//int main()
//{
//	struct Stu s = {"zhangsan", 20};
//	print(&s);//结构体指针传参
//	return 0;
//}

//struct Point
//{
//	int x;
//	int y;
//}p1 = { 1, 2 }; //第一种方法，这里创建的变量是全局变量，同时也能初始化               
//
//int main()
//{
//	struct Point p2 = { 3, 4 }; //第二种方法，这里创建的变量是局部变量，同时初始化
//
//	struct Point p3 = { .y = 5, .x = 6 }; //这里用了结构成员访问符，可以不按顺序初始化
//
//	return 0;
//}