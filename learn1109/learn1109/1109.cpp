#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
using namespace std;

//int a = 0;
//
//namespace haha
//{
//	int a = 1;
//}
//
//int main()
//{
//	int a = 2;
//	cout << a << endl;
//	cout << ::a << endl;
//	cout << haha :: a << endl;
//
//	return 0;
//}

//typedef struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//}st;
//
//void InitStack(st* pst, int defaultcapacity = 4)
//{
//	pst->a = (int*)malloc(sizeof(int) * defaultcapacity);
//	if (pst->a == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//
//	pst->top = 0;
//	pst->capacity = defaultcapacity;
//}
//
//int main()
//{
//	//指定初始化100个数据
//	st stk1;
//	InitStack(&stk1, 100);
//
//	//不指定初始化的数据，使用默认的缺省参数“4”
//	st stk2;
//	InitStack(&stk2);
//	return 0;
//}
//void Swap(int& x, int& y)
//{
//	int tmp = x;
//	x = y;
//	y = tmp;
//}
//
//int main()
//{
//	int a = 1, b = 3;
//	Swap(a, b);
//
//	cout << a << " " << b << endl;
//
//	return 0;
//}






struct SeqList
{
	int a[100];
	size_t size;
};

int SLGet(SeqList* ps, int pos)
{
	assert(pos < 100 && pos >= 0);

	return ps->a[pos];
}

void SLModify(SeqList* ps, int pos, int x)
{
	assert(pos < 100 && pos >= 0);

	ps->a[pos] = x;
}

int& SLAt(SeqList& s, int pos)
{
	assert(pos < 100 && pos >= 0);

	return s.a[pos];
}

int main()
{
	
	SeqList s;
	SLModify(&s, 0, 1);
	cout << SLGet(&s, 0) << endl;

	// 对第0个位的值+5
	int ret1 = SLGet(&s, 0);
	SLModify(&s, 0, ret1+5);

	SLAt(s, 0) = 1;
	cout << SLAt(s, 0) << endl;
	SLAt(s, 0) += 5;

	return 0;
}