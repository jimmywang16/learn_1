#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<set>
using namespace std;

int main()
{
	map<string, string> m;
	pair<string, string> p("string", "�ַ���");
	m.insert(p);

	m.insert(make_pair("insert", "����"));
	m["left"] = "��";
	m["right"] = "��";

	map<string, string>::iterator mit = m.begin();
	while (mit != m.end())
	{
		cout << mit->first << " : " << mit->second << endl;
		++mit;
	}
	cout << endl;

	auto mit2 = m.begin();
	while (mit2 != m.end())
	{
		cout << (*mit2).first << " : " << (*mit2).second << endl;
		++mit2;
	}

	cout << endl;
	set<int> s;
	s.insert(5);
	s.insert(6);
	s.insert(7);
	s.insert(8);
	s.insert(9);

	set<int>::iterator sit = s.begin();
	while (sit != s.end())
	{
		cout << *sit << endl;
		++sit;
	}

	return 0;
}