#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//
//class B
//{
//public:
//	//初始化列表：对象的成员定义的位置
//	B(int a, int ref)
//		:_aobj(a)
//		,_ref(ref)
//		,_n(10)
//
//	{}
//private:
//	//声明
//	A _aobj;      // 没有默认构造函数
//
//	// 特征：必须在定义的时候初始化
//	int& _ref;	  // 引用
//	const int _n; // const
//
//	int _x = 1;	  // 这里1是缺省值，缺省值是给初始化列表的
//};
//int main()
//{
//	// 对象整体定义
//
//
//	return 0;
//}
class Stack
{
public:
	Stack(size_t capacity = 10)
	{
		_a = (int*)malloc(capacity * sizeof(int));
		if (nullptr == _a)
		{
			perror("malloc申请空间失败");
			return;
		}
		_size = 0;
		_capacity = capacity;
	}
	void Push(const int& data)
	{
		// CheckCapacity();
		_a[_size] = data;
		_size++;
	}
	~Stack()
	{
		if (_a)
		{
			free(_a);
			_a = nullptr;
			_capacity = 0;
			_size = 0;
		}
	}
private:
	int* _a;
	int _size;
	int _capacity;
};

class MyQueue
{
public:
	MyQueue()
	{}

	MyQueue(int capacity)
		:_pushst(capacity)
		,_popst(capacity)

	{}

private:
	Stack _pushst;
	Stack _popst;
	int _x = 6;
};
int main()
{
	MyQueue q1;
	MyQueue q2(25);
	return 0;
}
