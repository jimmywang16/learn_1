#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>
#include <iostream>
using namespace std;

//typedef int DataType;
//class Stack
//{
//public:
//	Stack(DataType* a, int n)
//	{
//		cout << "Stack(DataType* a, int n)" << endl;
//		_array = (DataType*)malloc(sizeof(DataType) * n);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//		memcpy(_array, a, sizeof(DataType) * n);
//
//		_capacity = n;
//		_size = n;
//	}
//
//	Stack(int capacity = 4)
//	{
//		cout << "Stack(int capacity = 4)" << endl;
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//
//		_capacity = capacity;
//		_size = 0;
//	}
//
//	/*void Init()
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * 4);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//
//		_capacity = 4;
//		_size = 0;
//	}*/
//
//	void Push(DataType data)
//	{
//		CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	void Pop()
//	{
//		if (Empty())
//			return;
//		_size--;
//	}
//
//	DataType Top() { return _array[_size - 1]; }
//	int Empty() { return 0 == _size; }
//	int Size() { return _size; }
//
//	//void Destroy()
//	//{
//	//	if (_array)
//	//	{
//	//		free(_array);
//	//		_array = NULL;
//	//		_capacity = 0;
//	//		_size = 0;
//	//	}
//	//}
//
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//		if (_array)
//		{
//			free(_array);
//			_array = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//
//private:
//	void CheckCapacity()
//	{
//		if (_size == _capacity)
//		{
//			int newcapacity = _capacity * 2;
//			DataType* temp = (DataType*)realloc(_array, newcapacity * sizeof(DataType));
//			if (temp == NULL)
//			{
//				perror("realloc申请空间失败!!!");
//				return;
//			}
//			_array = temp;
//			_capacity = newcapacity;
//		}
//	}
//private:
//	DataType* _array;
//	int _capacity;
//	int _size;
//};
//
//
//class Date
//{
//public:
//	// 构成函数重载
//	// 但是无参调用存在歧义？
//	/*Date()
//	{
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}*/
//
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	// 内置类型
//	// C++11支持，这里不是初始化，因为这里只是声明
//	// 这里给的是默认的缺省值，给编译器生成默认构造函数用
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//
//	// 自定义类型
//	//Stack _st;
//};

#include <iostream>
#include <stdlib.h>
using namespace std;

class Date
{
public:

	//C++11加了补丁，可以给构造函数缺省值
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	void Print()
	{
		cout << _year << " / " << _month << " / " << _day << endl;
	}

private:
	//内置类型
	int _year = 6;
	int _month = 6;
	int _day = 6;
};

int main()
{
	//构造函数的调用和普通函数不一样
	Date da();
	//da.Print();

	//Date da2(2023, 1, 23);
	//da2.Print();
	return 0;
}

