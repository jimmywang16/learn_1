#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//void menu()
//{
//	printf("*************************\n");
//	printf("******1,add   2,sub******\n");
//	printf("******3,mul   4,div******\n");
//	printf("******0,exit	   ******\n");
//	printf("*************************\n");
//	printf("*************************\n");
//
//}
//int	Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//int(*pf[5])(int, int) = {0, Add, Sub, Mul, Div};
//
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	do
//	{
//		menu();
//		printf("请输入数字:>");
//		scanf("%d", &input);
//		if (input == 0)
//		{
//			printf("退出计算器\n");
//			break;
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数：>");
//			scanf("%d %d", &x, &y);
//			int ret = pf[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("输入错误\n");
//		}
//		
//
//	} while (input);
//	return 0;
//}
struct Stu
{
	char name[20];
	int age;
};

int cmp_s(const void* e1, const void* e2)
{
	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
}



void test2()
{
	struct Stu s[3] = { {"zhangsan", 20}, {"wangju", 58}, {"santatim", 11} };
	int sz = sizeof(s) / sizeof(s[0]);
	qsort(s, sz, sizeof(s[0]),cmp_s);
}

int main()
{
	test2();
	return 0;
}