#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <assert.h>

////1.指针减指针
//int my_strlen1(char arr[])
//{
//	assert(arr);
//	char* start = arr;
//	int count = 0;
//	while (*arr)
//	{
//		arr++;
//	}
//	return arr-start;
//}
//
////2.使用count++计算
//int my_strlen2(char arr[])
//{
//	assert(arr);
//	int count = 0;
//	while (*arr)
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//
////3.递归
//int my_strlen3(char arr[])
//{
//	assert(arr);
//	if (*arr != '\0')
//		return 1 + my_strlen3(arr + 1);
//	else
//		return 0;
//}
//int main()
//{
//	char arr[] = "hello world";
//	int ret1 = my_strlen1(arr);
//	int ret2 = my_strlen2(arr);
//	int ret3 = my_strlen3(arr);
//	printf("%d ", ret1);
//	printf("%d ", ret2);
//	printf("%d ", ret3);
//	return 0;
//}

//char* my_strcpy(char arr1[], char arr2[])
//{
//	assert(arr1 && arr2);
//	char* start = arr1;
//	while (*arr1++ = *arr2++)
//	{
//		;
//	}
//	return start;
//}
//
//int main()
//{
//	char arr1[] = "xxxxxxxxxxxxxxxxx";
//	char arr2[] = "hello world";
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}
//6666666666666666666666666666666666666666666666666666666666666666666666666666666
//char* my_strcat(char* dest, char* src)
//{
//	assert(dest && src);
//	char* start = dest;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return start;
//}
//
//int main()
//{
//	char arr1[] = "hello_";
//	char arr2[] = "world";
//	my_strcat(arr1, arr2);
//	printf("%s\n",arr1);
//	return 0;
//}

//666666666666666666666666666666666666666666666666666666666666666666666666666666666

//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//		return 1;
//	else
//		return -1;
//}
//
//int main()
//{
//	char arr1[] = "helloworld";
//	char arr2[] = "hellowoooo";
//	int ret = my_strcmp(arr1,arr2);
//	printf("%d ", ret);
//	return 0;
//}

///6666666666666666666666666666666666666666666666666666666666666666666666666666


//char* my_strstr(char* str1, char* str2)
//{
//	assert(str1 && str2);
//	char* s1 = str1;
//	char* s2 = str2;
//	char* cp = str1;
//	while (*cp)
//	{
//		s2 = str2;
//		s1 = cp;
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//			return cp;
//		cp++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abcdefghijklmnopq";
//	char arr2[] = "ghijk";
//	char* ptr = my_strstr(arr1, arr2);
//	if (ptr == NULL)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("%s\n", ptr);
//	}
//	return 0;

//666666666666666666666666666666666666666666666666666666666666666666666666666666666666666

//int main()
//{
//	printf("%s\n", strerror(0));
//	printf("%s\n", strerror(1));
//	printf("%s\n", strerror(2));
//	printf("%s\n", strerror(3));
//	printf("%s\n", strerror(4));
//	printf("%s\n", strerror(5));
//	printf("%s\n", strerror(6));
//	printf("%s\n", strerror(7));
//	return 0;
//}


//666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666

//void* my_memcpy(void* dest, void* src,size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		++(char*)dest;
//		++(char*)src;
//
//	}
//	return ret;
//}

//66666666666666666666666666666666666666666666666666666666666666666666666666666666666
void* my_memmove(void* dest, void* src, size_t num)
{
	assert(dest && src);
	if (dest > src)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			++(char*)dest;
			++(char*)src;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src+ num);
		}
	}
}


int main()
{
	char arr1[20] = { 0 };
	char arr2[20] = "helloworld";
	my_memmove(arr1, arr2, 6);
	printf("%s\n", arr1);
	return 0;
}


//博采众长，实践是检验真理的唯一标准
//	jimmywang16
//	2024/8/27	19:20