#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//void*可接收任意类型元素地址，但无法对其进行解引用操作
int cmp_t(void* e1, void* e2)
{
	//使用前按照比较的内容进行“强制类型转换”，再解引用操作
	return *(int*)e1 - *(int*)e2;
}

//由于前一步已经进行char*的强制类型转换，所以直接用char*类型接收
void swap(char* buf1, char* buf2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}

void bubble_sort(void *base, int num, int width,
	int(*compare)(void* e1, void* e2))
{
	int i = 0;
	//比较趟数
	for (i = 0; i < num - 1; i++)
	{
		int j = 0;
		//相邻两数比较，每次比完少一个数
		for (j = 0; j < num - 1 - i; j++)
		{
			//函数的新比较逻辑
			//先对base强制类型转换为char*类型
			//然后可依照不同类型元素加不同的width进行“不同类型的相邻元素”比较
			if (compare((char*)base + j*width, (char*)base + (j + 1)*width)>0)
			{
				//创建swap函数进行“单一字节交换”
				swap((char*)base + j*width, (char*)base + (j + 1)*width, width);
			}
		}
	}
}
int main()
{
	int arr[10] = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_t);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}
