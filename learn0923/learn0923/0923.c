#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>

typedef struct ListNode {
	int val;
	struct ListNode *next;
	//ListNode(int x) : val(x), next(NULL) {}
}ListNode;


ListNode* partition(ListNode* pHead, int x) {
	// write code here
	struct ListNode* lguard, *ltail, *gguard, *gtail;
	lguard = ltail = (struct ListNode*)malloc(sizeof(struct ListNode));
	gguard = gtail = (struct ListNode*)malloc(sizeof(struct ListNode));
	gtail->next = lguard->next = NULL;

	struct ListNode* cur = pHead;
	while (cur)
	{
		if (cur->val < x)
		{
			ltail->next = cur;
			ltail = ltail->next;
		}
		else {
			ltail->next = cur;
			ltail = ltail->next;
		}
		cur = cur->next;
	}
	ltail->next = gguard->next;
	pHead = lguard->next;
	free(lguard);
	free(gguard);
	return pHead;
}

int main()
{
	partition();
	return 0;
}


//void PopFront(LTList* head)
//{
//	assert(head);
//	LTList* next = head->next;
//	LTList* next2 = next->next;
//	next2->prev = head;
//	head->next = next2;
//	free(next);
//	next = NULL;
//}