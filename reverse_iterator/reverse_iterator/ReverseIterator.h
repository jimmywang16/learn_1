#pragma once

namespace a
{
	template<class Iterator, class Ref, class Ptr>
	struct ReverseIterator
	{
		typedef ReverseIterator<Iterator, Ref, Ptr> self;
		Iterator _it;

		ReverseIterator(Iterator it)
			:_it(it)
		{}

		Ref operator* ()
		{
			Iterator tmp(_it);
			return *(--tmp);
		}

		self& operator++()
		{
			--_it;
			return *this;
		}
		self& operator--()
		{
			++_it;
			return *this;
		}
		bool operator!=(const self& s)const
		{
			return _it != s._it;
		}

		Ptr operator->()
		{
			return &(operator*());
		}
	};
}
