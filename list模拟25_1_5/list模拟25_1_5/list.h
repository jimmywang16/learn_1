#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

namespace a
{
	template<class T>
	struct list_node
	{
		list_node<T>* _prev;
		list_node<T>* _next;
		T _val;

		list_node(const T& val = T())
			:_prev(nullptr)
			,_next(nullptr)
			,_val(val)
		{}
	};

	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef list_node<T> Node;
		Node* _node;
		typedef __list_iterator<T, Ref, Ptr> self;

		__list_iterator(Node* node)
			:_node(node)
		{}

		Ref operator*()
		{
			return _node->_val;
		}

		Ptr operator->()
		{
			return &_node->_val;
		}

		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}

		bool operator==(const self& it)const
		{
			return _node == it._node;
		}
		bool operator!=(const self& it)const
		{
			return _node != it._node;
		}

	};

	template<class T>
	class list
	{
		typedef list_node<T> Node;
	public:
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;

		iterator begin()
		{
			return _head->_next;
		}
		iterator end()
		{
			return _head;
		}
		const_iterator begin()const
		{
			return _head->_next;
		}
		const_iterator end()const
		{
			return _head;
		}

		void empty_init()
		{
			_head = new Node;
			_head->_prev = _head;
			_head->_next = _head;

			_size = 0;
		}

		list()
		{
			empty_init();
		}
		list(const list<T>& lt)
		{
			empty_init();

			for (auto e : lt)
			{
				push_back(e);
			}
		}

		void swap(list<T>& lt)
		{
			std::swap(_head, lt._head);
			std::swap(_size, lt._size);
		}

		list<T>& operator=(list<T> lt)
		{
			swap(lt);

			return *this;
		}

		~list()
		{
			clear();

			delete _head;
			_head = nullptr;
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}

			_size = 0;
		}

		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* newnode = new Node(x);

			prev->_next = newnode;
			newnode->_prev = prev;
			
			newnode->_next = cur;
			cur->_prev = newnode;

			++_size;
			return newnode;
		}

		void push_back(const T& x)
		{
			insert(end(), x);
		}
		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		void pop_front()
		{
			erase(begin());
		}
		void pop_back()
		{
			erase(--end());
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* next = cur->_next;
			
			prev->_next = next;
			next->_prev = prev;
			delete cur;
			--_size;

			return next;
		}
		
		size_t size()
		{
			return _size;
		}
	private:
		Node* _head;
		size_t _size;
	};

	void test1()
	{
		list<int> lt1;
		
		
		lt1.push_back(0);
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		lt1.push_back(5);

		list<int>::iterator it = lt1.begin();
		while (it != lt1.end())
		{
			cout << *it << " ";
			++it;
		}/**/
		cout << endl;

		lt1.push_front(-1);
		lt1.push_front(-2);
		lt1.push_front(-3);
		lt1.push_front(-4);
	
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		lt1.pop_back();
		lt1.pop_back();
		lt1.pop_front();

		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;


		list<int> lt2(lt1);
		lt2.push_back(6);
		lt2.push_back(7);
		cout << "lt2 : ";
		for (auto e : lt2)
		{
			cout << e << " ";
		}
		cout << endl;


		lt1 = lt2;
		cout << " lt1 : ";
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		lt1.pop_front();
		lt1.pop_front();
		lt1.pop_back();
		lt1.pop_back();
		
		cout << " lt1 : ";
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		lt1.erase(lt1.begin());
		lt1.erase(--lt1.end());
		cout << " lt1 : ";
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int>::iterator endit = --lt2.end();
		while (endit != lt2.end())
		{
			cout << *endit << " ";
			endit--;
		}
	}
}
