#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stdlib.h>
using namespace std;

//class Date
//{
//public:
//	
//	//C++11加了补丁，可以给构造函数缺省值
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << " / " << _month << " / " << _day << endl;
//	}
//
//private:
//	//内置类型
//	int _year = 6;
//	int _month = 6;
//	int _day = 6;
//};
//
//int main()
//{
//	//构造函数的调用和普通函数不一样
//	Date da();
//	da().Print();
//
//	Date da2(2023, 1, 23);
//	da2.Print();
//	return 0;
//}

