#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stdlib.h>
//#include <string.h>
using namespace std;
//
//class Date
// //{
//public:
//	Date(int year = 2013, int month = 5, int day = 10)
//	{
//		cout << "123use" << endl;
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	/*void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	//d1.Init(2022, 7, 5);
//	d1.Print();
//	Date d2;
//	//d2.Init(2022, 7, 6);
//	d2.Print();
//	return 0;
//}

typedef int DataType;
class Stack
{
public:

	Stack(int capacity = 4)
	{
		cout << "use Stack()" << endl;
		_a = (int*)malloc(sizeof(int) * capacity);
		if (nullptr == _a)
		{
			perror("malloc申请空间失败!!!");
			return;
		}
		_capacity = capacity;
		_size = 0;
	}

	Stack(const Stack& st)
	{
		_a = (int*)malloc(sizeof(int) * st._capacity);
		if (nullptr == _a)
		{
			perror("malloc was failed");
			return;
		}

		memcpy(_a, st._a, sizeof(int) * st._size);
		_capacity = st._capacity;
		_size = st._size;
	}

	void Push(int data)
	{
		// CheckCapacity();
		_a[_size] = data;
		_size++;
	}

	~Stack()
	{
		cout << "use ~Stack()" << endl;
		free(_a);
		_a = nullptr;
		_capacity = 0;
		_size = 0;
	}
private:
	int* _a;
	int _capacity;
	int _size;
};

int main()
{
	Stack st1;
	Stack st2(st1);

	return 0;
}


//class Date
//{
//public:
//	Date()
//	{
//		_year = 3;
//		_month = 3;
//		_day = 3;
//	}
//
//	//C++11加了补丁，可以给构造函数缺省值
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << " / " << _month << " / " << _day << endl;
//
//	}
//
//private:
//	//内置类型
//	int _year;
//	int _month;
//	int _day;
//
//	//自定义类型
//	//Stack _st;
//};
//
//int main()
//{
//	Date da;
//
//	//构造函数的调用和普通函数不一样
//	//Date da1;
//
//	da.Print();
//	//da1.Print();
//
//	return 0;
//}
//
//void TestStack()
//{
//	//Stack s1;
//	////s.Push(1);
//	////s.Push(2);
//
//	//Stack s2;
//	//Stack s3;
//}

//int main()
//{
//	TestStack();
//
//	return 0;
//}


//class Time
//{
//public:
//	~Time()
//	{
//		cout << "~Time()" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//private:
//	// 基本类型(内置类型)
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//	// 自定义类型
//	Time _t;
//};
//
//int main()
//{
//	Date d;
//	//监视可查看
//	return 0;
//}