#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_STUDENTS 100
#define MAX_SUBJECTS 10

typedef struct Student
{
	int id;
	char name[32];
	char classname[32];
	int subjectcount;
	struct subject
	{
		char name[32];
		int score;
	}subject[MAX_SUBJECTS];

}Student;

int loadStudent(Student* students)
{
	FILE* fp = fopen("student.dat", "rb");
	if (fp != NULL)
	{
		int actualStudentCount = 0;
		while (1)
		{
			Student student;
			size_t bytesRead = fread(&student, sizeof(Student), 1, fp);
			if (bytesRead == 0)
			{
				break;
			}
			students[actualStudentCount] = student;
			++actualStudentCount;
		}
		fclose(fp);
		return actualStudentCount;
	}
	else
	{
		printf("没有找到文件，系统将新建文件\n");
		return 0;
	}
}

Student* findStudent_id(Student* students, int studentcount, int id)
{
	for (int i = 0; i < studentcount; ++i)
	{
		if (students[i].id == id)
		{
			return &students[i];
		}

	}
	return NULL;
}

void addStudent(Student* students, int* studentcount)
{
	//判断当前是否达到最大值
	if (*studentcount >= MAX_STUDENTS)
	{
		printf("已经达到最大数量上限，无法添加\n");
		return;
	}
	Student* newstudent = &students[*studentcount];

	printf("输入学生信息：\n");
	//1
	printf("学号->");
	while (1)
	{
		int id;
		scanf("%d", &id);
		getchar();

		if (findStudent_id(students, *studentcount, id) == NULL)
		{
			newstudent->id = id;
			break;
		}
		else
		{
			printf("学号已经存在，请重新输入->");
		}
	}
	//2
	printf("姓名->");
	gets(newstudent->name);
	//3
	printf("班级->");
	gets(newstudent->classname);
	//4
	printf("学科数量->");
	while (1)
	{
		int subjectcount = 0;
		scanf("%d", &subjectcount);
		getchar();

		if (subjectcount > 0 && subjectcount <= MAX_SUBJECTS)
		{
			newstudent->subjectcount = subjectcount;
			break;
		}
		else
		{
			printf("学科数量不合法，请重新输入->");
		}
	}

	//5
	printf("请输入学科信息:\n");
	for (int i = 0; i < newstudent->subjectcount; ++i)
	{
		printf("请输入第%d门学科信息：\n", i + 1);

		printf("学科名称");
		gets(newstudent->subject[i].name);

		//输入score
		printf("成绩->");
		while (1)
		{
			int score;
			scanf("%d", &score);
			getchar();

			if (score >= 0 && score <= 100)
			{
				newstudent->subject[i].score = score;
				break;
			}
			else
			{
				printf("\n成绩分数不合法，重新输入->");
			}
		}
	}

	++(*studentcount);
}

void deleteStudent(Student* students, int* studentcount)
{
	if (*studentcount == 0)
	{
		printf("还没有学生信息！\n");
		return;
	}

	printf("请输入删除的学生学号->");
	int id;
	scanf("%d", &id);
	int found = 0;
	for (int i = 0; i < *studentcount; ++i)
	{
		if (students[i].id == id)
		{
			for (int j = 0; j < *studentcount - 1; ++j)
			{
				students[j] = students[j + 1];

			}
			--(*studentcount);
			found = 1;
			break;
		}
	}
	if (found == 0)
	{
		printf("没找到该学生信息!!!\n");
	}

}

void modifyStudent(Student* students, int* studentcount)
{
	if (*studentcount == 0)
	{
		printf("没找到该学生信息!!!\n");
		return;
	}
	printf("请输入要修改的学生学号->");
	int id = 0;
	scanf("%d", &id);
	int found = 0;

	for (int i = 0; i < *studentcount; ++i)
	{
		if (students[i].id == id)
		{
			printf("请输入学生信息：\n");
			getchar();

			printf("请输入学生姓名->");
			gets(students[i].name);

			printf("请输入学生班级->");
			gets(students[i].classname);

			printf("请输入学科数量->");
		
			while (1)
			{
				int subjectcount = 0;
				scanf("%d", &subjectcount);
				getchar();

				if (subjectcount > 0 && subjectcount <= MAX_SUBJECTS)
				{
					students[i].subjectcount = subjectcount;
					break;
				}
				else
				{
					printf("学科数量不合法，请重新输入->");
				}
			}

			printf("请输入学科信息:\n");
			for (int j = 0; j < students[i].subjectcount; ++j)
			{
				printf("请输入第%d门学科信息：\n", j + 1);

				printf("学科名称");
				gets(students[i].subject[j].name);

				//输入score
				printf("成绩->");
				while (1)
				{
					int score;
					scanf("%d", &score);
					getchar();

					if (score >= 0 && score <= 100)
					{
						students[i].subject[j].score = score;
						break;
					}
					else
					{
						printf("\n成绩分数不合法，重新输入->");
					}
				}
			}
			found = 1;
			break;
		}
	}
	if (found == 0)
	{
		printf("没找到该学生信息!!!\n");
	}

}

void findStudent(Student* students, int* studentcount)
{
	if (*studentcount == 0)
	{
		printf("学生信息为0，查询失败\n");
		return;
	}

	printf("请输入查询的学生学号->");
	int id;
	scanf("%d", &id);
	int found = 0;

	for (int i = 0; i < *studentcount; ++i)
	{
		if (students[i].id == id)
		{
			printf("学生信息如下：");
			printf("学号:%d\n", students[i].id);
			printf("姓名:%s\n", students[i].name);
			printf("班级:%s\n", students[i].classname);
			printf("学科数量:%d\n", students[i].subjectcount);

			for (int j = 0; j < students[i].subjectcount; ++j)
			{
				printf("第%d门学科：\n", j + 1);
				printf("学科名称:%s\n", students[i].subject[j].name);
				printf("学科成绩:%d\n", students[i].subject[j].score);
			}
			found = 1;
			break;
		}
	}
	if (found == 0)
	{
		printf("没有找到该学生信息！\n");
	}
}

int login()
{
	printf("******欢迎来到信息管理系统******\n");

	char user[20];
	char password[20];

	printf("请输入登陆账户:\n");
	gets(user);

	printf("请输入密码:\n");
	gets(password);

	if (strcmp(user, "admin") == 0 && strcmp(password, "123456") == 0)
	{
		printf("登陆成功\n");
		return 1;
	}
	else
	{
		printf("登录失败\n");
		return 0;
	}
}

void menu()
{
	printf("******管理系统菜单******\n");
	printf("******************\n");
	printf("******1、添加学生******\n");
	printf("******2、删除学生******\n");
	printf("******3、修改学生******\n");
	printf("******4、查询学生******\n");
	printf("******5、退出学生******\n");
	printf("******************\n");


}

void headchoice(Student* students, int* studentcount)
{
	int choice;

	do
	{
		menu();
		printf("请输入您的选择：\n");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1:
			printf("这是添加功能\n");
			addStudent(students, studentcount);
			break;
		case 2:
			printf("这是删除功能\n");
			deleteStudent(students, studentcount);
			break;
		case 3:
			printf("这是修改功能\n");
			modifyStudent(students, studentcount);
			break;
		case 4:
			printf("这是查询功能\n");
			findStudent(students, studentcount);
			break;
		case 5:
			
			break;
		default:
			printf("输入错误，请重新输入\n");
		}

		if (choice == 5)
		{
			printf("退出成功\n");
			break;
		}


	} while (choice != 16);

	
}


int main()
{
	Student students[MAX_STUDENTS];
	int studentcount = 0;

	studentcount = loadStudent(students);


	if (!login())
	{
		printf("登录失败，退出系统\n");
		return 1;
	}
	

	headchoice(students, &studentcount);

	return 0;
}