#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void reverse(int* nums, int begin, int end)
{
	while (begin < end)
	{
		int tmp = nums[begin];
		nums[begin] = nums[end];
		nums[end] = tmp;

		begin++;
		end--;
	}
}

void rotate(int* nums, int numsSize, int k) 
{
	if (k > numsSize)
	{
		k %= numsSize;
	}
	reverse(nums, 0, numsSize - k - 1);
	reverse(nums, numsSize - k, numsSize - 1);
	reverse(nums, 0, numsSize - 1);

}

int main()
{
	int nums[7] = { 1,2,3,4,5,6,7 };
	int sz = sizeof(nums) / sizeof(nums[0]);
	int k = 0;
	scanf("%d", &k);
	rotate(nums, sz, k);
	int i = 0;
	for (i = 0; i < 7; i++)
	{
		printf("%d ", nums[i]);
	}
	return 0;
}