#define _CRT_SECURE_NO_WARNINGS 1
#include "func.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>



int main()
{
	struct Stack st1;
	st1.Init(20);

	Stack st2;
	st2.Init();
	st2.Push(1);
	st2.Push(2);
	
	st2.Destroy();

	return 0;
}