#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

class Stack
{
public:
	//成员函数
	/*void Init(int defaultcapacity = 4)
	{
		a = (int*)malloc(sizeof(int) * defaultcapacity);
		if (a == nullptr)
		{
			perror("malloc fail");
			return;
		}

		_capacity = defaultcapacity;
		_top = 0;
	}*/

	//声明和定义不能同时给缺省参数，缺省参数一般给声明
	void Init(int defaultcapacity = 4);
	
	//C++里面长的函数声明和定义分离，短的函数直接写在类里面
	//类里面定义的函数默认就是inline
	void Push(int x)
	{
		a[_top++] = x;
	}

	void Destroy()
	{
		free(a);
		a = nullptr;
		_top = 0;
		_capacity = 0;
	}

	int Top()
	{
		return a[_top-1];
	}

	//成员变量
private:
	int* a;
	int _top;
	int _capacity;
};
