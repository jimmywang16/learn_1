#define _CRT_SECURE_NO_WARNINGS 1
#include "func.h"

void Stack::Init(int defaultcapacity)
{
	a = (int*)malloc(sizeof(int) * defaultcapacity);
	if (a == nullptr)
	{
		perror("malloc fail");
		return;
	}

	_capacity = defaultcapacity;
	_top = 0;
}