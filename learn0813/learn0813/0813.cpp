#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//int add(int x, int y)
//{
//	return x + y;
//}
//int sub(int x, int y)
//{
//	return x + y;
//}
//int mul(int x, int y)
//{
//	return x*y;
//}
//int main()
//{
//	int x = 0;
//	int y = 0;
//	int ret = add(x, y);
//	//int (*pf)(int, int) = &add;
//	int(*pf[3])(int, int) = {add, sub, mul};
//	int(*(*ppf)[3])(int, int) = &pf;
//	return 0;
//}
void bubble_sort(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}

}
int main()
{
	int arr[10] = { 8, 7, 9, 6, 5, 4, 1, 2, 3, 0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d", arr[i]);
	}
	return 0;
}