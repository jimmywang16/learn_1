#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{	//1.初始化足够大的二维数组
	int arr[10][10] = {0};
	//2.接收数据
	printf("请输入正方形矩阵的行和列：");
	int a = 0;
	scanf("%d", &a);
	int i = 0, j = 0;
	for (i = 0; i < a; i++)
	{
		for (j = 0; j < a; j++)
		{
			int b = 0;
			scanf("%d", &b);
			arr[i][j] = b;
		}
	}
	printf("---------------------------------------\n");
	//3.展示数字矩阵
	printf("矩阵如下所示：\n");
	for (i = 0; i < a; i++)
	{
		for (j = 0; j < a; j++)
		{
			printf("%-3d ", arr[i][j]);
		}
		printf("\n");
	}
	//4.对角线相加
	int sum1 = 0;
	for (i = 0; i < a; i++)
	{
		j = i;
		sum1 += arr[i][j];
	}
	printf("左上到右下：%d\n", sum1);
	int sum2 = 0;
	for (i = a - 1; i >= 0; i--)
	{
		j = a - i - 1;
		sum2 += arr[i][j];

	}
	printf("右上到左下：%d\n", sum2);
	int sum = sum1 + sum2;
	//5.判断行列是否为奇数
	if (a % 2 == 1)
	{
		int p = a / 2;
		sum -= arr[p][p];
	}
	//6.输出
	printf("结果为：%d\n", sum);
	return 0;
}