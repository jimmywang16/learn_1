#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int b = 14;
int a = 12;
int main()
{
	//局部变量,当全局变量和局部变量同名冲突，局部优先
	int a = 15;
	printf("%d\n", a);
	printf("%d\n", b);
	return 0;
}